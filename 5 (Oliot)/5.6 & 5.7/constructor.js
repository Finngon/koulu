var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var CAjoneuvo = /** @class */ (function () {
    function CAjoneuvo() {
    }
    Object.defineProperty(CAjoneuvo.prototype, "iIskutilavuus", {
        get: function () {
            return this._iIskutilavuus;
        },
        set: function (uusiIsku) {
            this._iIskutilavuus = uusiIsku;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iHinta", {
        get: function () {
            return this._iHinta;
        },
        set: function (x) {
            this._iHinta = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iOvienLkm", {
        get: function () {
            return this._iOvienLkm;
        },
        set: function (x) {
            this._iOvienLkm = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "bKattoikkuna", {
        get: function () {
            return this._bKattoikkuna;
        },
        set: function (x) {
            this._bKattoikkuna = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iAjetutKilometrit", {
        get: function () {
            return this._iAjetutKilometrit;
        },
        set: function (x) {
            this._iAjetutKilometrit = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "sMerkki", {
        get: function () {
            return this._sMerkki;
        },
        set: function (x) {
            this._sMerkki = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iValmistusvuosi", {
        get: function () {
            return this._iValmistusvuosi;
        },
        set: function (x) {
            this._iValmistusvuosi = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "dMoottorinIskutilavuusLtr", {
        get: function () {
            return this._dMoottorinIskutilavuusLtr;
        },
        set: function (x) {
            this._dMoottorinIskutilavuusLtr = x;
        },
        enumerable: true,
        configurable: true
    });
    return CAjoneuvo;
}());
var CHalytysAjoneuvo = /** @class */ (function (_super) {
    __extends(CHalytysAjoneuvo, _super);
    function CHalytysAjoneuvo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CHalytysAjoneuvo.prototype, "bHalytys", {
        get: function () {
            return this._bHalytys;
        },
        set: function (x) {
            this._bHalytys = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CHalytysAjoneuvo.prototype, "bRadio", {
        get: function () {
            return this._bRadio;
        },
        set: function (x) {
            this._bRadio = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CHalytysAjoneuvo.prototype, "iKorkeus", {
        get: function () {
            return this._iKorkeus;
        },
        set: function (x) {
            this._iKorkeus = x;
        },
        enumerable: true,
        configurable: true
    });
    return CHalytysAjoneuvo;
}(CAjoneuvo));
var CPaloHalytysAjoneuvo = /** @class */ (function (_super) {
    __extends(CPaloHalytysAjoneuvo, _super);
    function CPaloHalytysAjoneuvo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CPaloHalytysAjoneuvo.prototype, "iTikkaatKorkeus", {
        get: function () {
            return this._iTikkaatKorkeus;
        },
        set: function (x) {
            this._iTikkaatKorkeus = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CPaloHalytysAjoneuvo.prototype, "iLetkuLkm", {
        get: function () {
            return this._iLetkuLkm;
        },
        set: function (x) {
            this._iLetkuLkm = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CPaloHalytysAjoneuvo.prototype, "iVesiTilavuus", {
        get: function () {
            return this._iVesiTilavuus;
        },
        set: function (x) {
            this._iVesiTilavuus = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CPaloHalytysAjoneuvo.prototype, "NykVesi", {
        get: function () {
            return this._NykVesi;
        },
        set: function (x) {
            if (x > 0) {
                this.bOnkoVetta = true;
                this._NykVesi = x;
            }
            else if (x === 0) {
                this.bOnkoVetta = false;
                this._NykVesi = x;
            }
            else {
                console.log("Ei voi olla negatiivista määrää vettä");
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CPaloHalytysAjoneuvo.prototype, "bOnkoVetta", {
        get: function () {
            return this._bOnkoVetta;
        },
        set: function (x) {
            this._bOnkoVetta = x;
        },
        enumerable: true,
        configurable: true
    });
    CPaloHalytysAjoneuvo.prototype.LisaaVesi = function (vesimaara) {
        if (vesimaara + this._NykVesi > this._iVesiTilavuus) {
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!";
        }
        else {
            this._NykVesi += vesimaara;
            if (this._bOnkoVetta === false) {
                this._bOnkoVetta = true;
            }
            document.getElementById("result2").innerHTML = "Lis\u00E4tty " + vesimaara.toFixed(2) + " litraa / " + (vesimaara * 0.22).toFixed(2) + " Englannin gallonaa / " + (vesimaara * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Vett\u00E4 on nyt " + this._NykVesi.toFixed(2) + " litraa / " + (this._NykVesi * 0.22).toFixed(2) + " Englannin gallonaa / " + (this._NykVesi * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Tilaa j\u00E4ljell\u00E4: " + (this._iVesiTilavuus - this._NykVesi).toFixed(2) + " litraa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2) + " Englannin gallonaa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2) + " Amerikan gallonaa.";
            if (this._bOnkoVetta === true) {
                document.getElementById("result").innerHTML += "Vett\u00E4 on tankissa";
            }
        }
    };
    CPaloHalytysAjoneuvo.prototype.PoistaVesi = function (vesimaara) {
        if (this._NykVesi - vesimaara < 0) {
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!";
        }
        else {
            this._NykVesi -= vesimaara;
            if (this._NykVesi === 0) {
                this._bOnkoVetta = false;
            }
            document.getElementById("result2").innerHTML = "Poistettu " + vesimaara.toFixed(2) + " litraa / " + (vesimaara * 0.22).toFixed(2) + " Englannin gallonaa / " + (vesimaara * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Vett\u00E4 on nyt " + this._NykVesi.toFixed(2) + " litraa / " + (this._NykVesi * 0.22).toFixed(2) + " Englannin gallonaa / " + (this._NykVesi * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Tilaa j\u00E4ljell\u00E4: " + (this._iVesiTilavuus - this._NykVesi).toFixed(2) + " litraa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2) + " Englannin gallonaa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2) + " Amerikan gallonaa.";
            if (this._bOnkoVetta === false) {
                document.getElementById("result").innerHTML += "Vett\u00E4 ei ole tankissa";
            }
        }
    };
    CPaloHalytysAjoneuvo.prototype.LisaaVesiUKGal = function (vesimaara) {
        vesimaara *= 4.546;
        if (vesimaara + this._NykVesi > this._iVesiTilavuus) {
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!";
        }
        else {
            this._NykVesi += vesimaara;
            if (this._bOnkoVetta === false) {
                this._bOnkoVetta = true;
            }
            document.getElementById("result2").innerHTML = "Lis\u00E4tty " + vesimaara.toFixed(2) + " litraa / " + (vesimaara * 0.22).toFixed(2) + " Englannin gallonaa / " + (vesimaara * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Vett\u00E4 on nyt " + this._NykVesi.toFixed(2) + " litraa / " + (this._NykVesi * 0.22).toFixed(2) + " Englannin gallonaa / " + (this._NykVesi * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Tilaa j\u00E4ljell\u00E4: " + (this._iVesiTilavuus - this._NykVesi).toFixed(2) + " litraa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2) + " Englannin gallonaa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2) + " Amerikan gallonaa.";
            if (this._bOnkoVetta === true) {
                document.getElementById("result").innerHTML += "Vett\u00E4 on tankissa";
            }
        }
    };
    CPaloHalytysAjoneuvo.prototype.PoistaVesiUKGal = function (vesimaara) {
        vesimaara *= 4.54;
        if (this._NykVesi - vesimaara < 0) {
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!";
        }
        else {
            this._NykVesi -= vesimaara;
            if (this._NykVesi === 0) {
                this._bOnkoVetta = false;
            }
            document.getElementById("result2").innerHTML = "Poistettu " + vesimaara.toFixed(2) + " litraa / " + (vesimaara * 0.22).toFixed(2) + " Englannin gallonaa / " + (vesimaara * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Vett\u00E4 on nyt " + this._NykVesi.toFixed(2) + " litraa / " + (this._NykVesi * 0.22).toFixed(2) + " Englannin gallonaa / " + (this._NykVesi * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Tilaa j\u00E4ljell\u00E4: " + (this._iVesiTilavuus - this._NykVesi).toFixed(2) + " litraa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2) + " Englannin gallonaa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2) + " Amerikan gallonaa.";
            if (this._bOnkoVetta === false) {
                document.getElementById("result").innerHTML += "Vett\u00E4 ei ole tankissa";
            }
        }
    };
    CPaloHalytysAjoneuvo.prototype.LisaaVesiUSGal = function (vesimaara) {
        vesimaara *= 3.78;
        if (vesimaara + this._NykVesi > this._iVesiTilavuus) {
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!";
        }
        else {
            this._NykVesi += vesimaara;
            if (this._bOnkoVetta === false) {
                this._bOnkoVetta = true;
            }
            document.getElementById("result2").innerHTML = "Lis\u00E4tty " + vesimaara.toFixed(2) + " litraa / " + (vesimaara * 0.22).toFixed(2) + " Englannin gallonaa / " + (vesimaara * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Vett\u00E4 on nyt " + this._NykVesi.toFixed(2) + " litraa / " + (this._NykVesi * 0.22).toFixed(2) + " Englannin gallonaa / " + (this._NykVesi * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Tilaa j\u00E4ljell\u00E4: " + (this._iVesiTilavuus - this._NykVesi).toFixed(2) + " litraa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2) + " Englannin gallonaa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2) + " Amerikan gallonaa.";
            if (this._bOnkoVetta === true) {
                document.getElementById("result").innerHTML += "Vett\u00E4 on tankissa";
            }
        }
    };
    CPaloHalytysAjoneuvo.prototype.PoistaVesiUSGal = function (vesimaara) {
        vesimaara *= 3.785;
        if (this._NykVesi - vesimaara < 0) {
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!";
        }
        else {
            this._NykVesi -= vesimaara;
            if (this._NykVesi === 0) {
                this._bOnkoVetta = false;
            }
            document.getElementById("result2").innerHTML = "Poistettu " + vesimaara.toFixed(2) + " litraa / " + (vesimaara * 0.22).toFixed(2) + " Englannin gallonaa / " + (vesimaara * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Vett\u00E4 on nyt " + this._NykVesi.toFixed(2) + " litraa / " + (this._NykVesi * 0.22).toFixed(2) + " Englannin gallonaa / " + (this._NykVesi * 0.26).toFixed(2) + " Amerikan gallonaa.<br/>\n            Tilaa j\u00E4ljell\u00E4: " + (this._iVesiTilavuus - this._NykVesi).toFixed(2) + " litraa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2) + " Englannin gallonaa / " + ((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2) + " Amerikan gallonaa.";
            if (this._bOnkoVetta === false) {
                document.getElementById("result").innerHTML += "Vett\u00E4 ei ole tankissa";
            }
        }
    };
    return CPaloHalytysAjoneuvo;
}(CHalytysAjoneuvo));
