class CAjoneuvo { 
    private _iIskutilavuus: number;
    private _iHinta: number;
    private _iOvienLkm: number;
    private _bKattoikkuna: boolean;
    private _iAjetutKilometrit: number;
    private _sMerkki: string;
    private _iValmistusvuosi: number;
    private _dMoottorinIskutilavuusLtr: number

    get iIskutilavuus(): number {
        return this._iIskutilavuus;
    }

    set iIskutilavuus(uusiIsku: number) {
        this._iIskutilavuus = uusiIsku
    }

    get iHinta(): number {
        return this._iHinta;
    }

    set iHinta(x: number) {
        this._iHinta = x
    }

    get iOvienLkm(): number {
        return this._iOvienLkm;
    }

    set iOvienLkm(x: number) {
        this._iOvienLkm = x
    }

    get bKattoikkuna(): boolean {
        return this._bKattoikkuna;
    }

    set bKattoikkuna(x: boolean) {
        this._bKattoikkuna = x
    }

    get iAjetutKilometrit(): number {
        return this._iAjetutKilometrit;
    }

    set iAjetutKilometrit(x: number) {
        this._iAjetutKilometrit = x
    }

    get sMerkki(): string {
        return this._sMerkki;
    }

    set sMerkki(x: string) {
        this._sMerkki = x
    }

    get iValmistusvuosi(): number {
        return this._iValmistusvuosi;
    }

    set iValmistusvuosi(x: number) {
        this._iValmistusvuosi = x
    }

    get dMoottorinIskutilavuusLtr(): number {
        return this._dMoottorinIskutilavuusLtr;
    }

    set dMoottorinIskutilavuusLtr(x: number) {
        this._dMoottorinIskutilavuusLtr = x
    }
}

class CHalytysAjoneuvo extends CAjoneuvo {
    private _bHalytys: boolean;
    private _bRadio: boolean;
    private _iKorkeus: number

    get bHalytys(): boolean {
        return this._bHalytys;
    }

    set bHalytys(x: boolean) {
        this._bHalytys = x
    }

    get bRadio(): boolean {
        return this._bRadio;
    }

    set bRadio(x: boolean) {
        this._bRadio = x
    }

    get iKorkeus(): number {
        return this._iKorkeus;
    }

    set iKorkeus(x: number) {
        this._iKorkeus = x
    }
}

class CPaloHalytysAjoneuvo extends CHalytysAjoneuvo {
    private _iTikkaatKorkeus: number;
    private _iLetkuLkm: number;
    private _iVesiTilavuus: number;
    private _NykVesi: number;
    private _bOnkoVetta: boolean;

    get iTikkaatKorkeus(): number {
        return this._iTikkaatKorkeus;
    }

    set iTikkaatKorkeus(x: number) {
        this._iTikkaatKorkeus = x
    }

    get iLetkuLkm(): number {
        return this._iLetkuLkm;
    }

    set iLetkuLkm(x: number) {
        this._iLetkuLkm = x
    }

    get iVesiTilavuus(): number {
        return this._iVesiTilavuus;
    }

    set iVesiTilavuus(x: number) {
        this._iVesiTilavuus = x
    }

    get NykVesi(): number {
        return this._NykVesi;
    }

    set NykVesi(x: number) {
        if(x > 0){
            this.bOnkoVetta = true;
            this._NykVesi = x
        } else if(x === 0){
            this.bOnkoVetta = false;
            this._NykVesi = x
        } else {
            console.log("Ei voi olla negatiivista määrää vettä")
        }
    }

    get bOnkoVetta(): boolean {
        return this._bOnkoVetta;
    }

    set bOnkoVetta(x: boolean) {
        this._bOnkoVetta = x
    }

    LisaaVesi(vesimaara: number){
        if(vesimaara + this._NykVesi > this._iVesiTilavuus){
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!"
        } else {
            this._NykVesi += vesimaara;
            if(this._bOnkoVetta === false){
                this._bOnkoVetta = true;
            }
            document.getElementById("result2").innerHTML = `Lisätty ${vesimaara.toFixed(2)} litraa / ${(vesimaara * 0.22).toFixed(2)} Englannin gallonaa / ${(vesimaara * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Vettä on nyt ${this._NykVesi.toFixed(2)} litraa / ${(this._NykVesi * 0.22).toFixed(2)} Englannin gallonaa / ${(this._NykVesi * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Tilaa jäljellä: ${(this._iVesiTilavuus - this._NykVesi).toFixed(2)} litraa / ${((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2)} Englannin gallonaa / ${((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2)} Amerikan gallonaa.`
            if (this._bOnkoVetta === true) {
                document.getElementById("result").innerHTML += `Vettä on tankissa`
            }
        }
    }

    PoistaVesi(vesimaara: number) {
        if(this._NykVesi - vesimaara < 0){
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!"
        } else {
            this._NykVesi -= vesimaara;
            if(this._NykVesi === 0){
                this._bOnkoVetta = false;
            }
            document.getElementById("result2").innerHTML = `Poistettu ${vesimaara.toFixed(2)} litraa / ${(vesimaara * 0.22).toFixed(2)} Englannin gallonaa / ${(vesimaara * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Vettä on nyt ${this._NykVesi.toFixed(2)} litraa / ${(this._NykVesi * 0.22).toFixed(2)} Englannin gallonaa / ${(this._NykVesi * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Tilaa jäljellä: ${(this._iVesiTilavuus - this._NykVesi).toFixed(2)} litraa / ${((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2)} Englannin gallonaa / ${((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2)} Amerikan gallonaa.`
            if(this._bOnkoVetta === false){
                document.getElementById("result").innerHTML += `Vettä ei ole tankissa`
            }
        }
    }

    LisaaVesiUKGal(vesimaara: number) {
        vesimaara *= 4.546
        if (vesimaara + this._NykVesi > this._iVesiTilavuus) {
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!"
        } else {
            this._NykVesi += vesimaara;
            if (this._bOnkoVetta === false) {
                this._bOnkoVetta = true;
            }
            document.getElementById("result2").innerHTML = `Lisätty ${vesimaara.toFixed(2)} litraa / ${(vesimaara * 0.22).toFixed(2)} Englannin gallonaa / ${(vesimaara * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Vettä on nyt ${this._NykVesi.toFixed(2)} litraa / ${(this._NykVesi * 0.22).toFixed(2)} Englannin gallonaa / ${(this._NykVesi * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Tilaa jäljellä: ${(this._iVesiTilavuus - this._NykVesi).toFixed(2)} litraa / ${((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2)} Englannin gallonaa / ${((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2)} Amerikan gallonaa.`
            if (this._bOnkoVetta === true) {
                document.getElementById("result").innerHTML += `Vettä on tankissa`
            }
        }
    }

    PoistaVesiUKGal(vesimaara: number) {
        vesimaara *= 4.54
        if (this._NykVesi - vesimaara < 0) {
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!"
        } else {
            this._NykVesi -= vesimaara;
            if (this._NykVesi === 0) {
                this._bOnkoVetta = false;
            }
            document.getElementById("result2").innerHTML = `Poistettu ${vesimaara.toFixed(2)} litraa / ${(vesimaara * 0.22).toFixed(2)} Englannin gallonaa / ${(vesimaara * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Vettä on nyt ${this._NykVesi.toFixed(2)} litraa / ${(this._NykVesi * 0.22).toFixed(2)} Englannin gallonaa / ${(this._NykVesi * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Tilaa jäljellä: ${(this._iVesiTilavuus - this._NykVesi).toFixed(2)} litraa / ${((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2)} Englannin gallonaa / ${((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2)} Amerikan gallonaa.`
            if (this._bOnkoVetta === false) {
                document.getElementById("result").innerHTML += `Vettä ei ole tankissa`
            }
        }
    }

    LisaaVesiUSGal(vesimaara: number) {
        vesimaara *= 3.78
        if (vesimaara + this._NykVesi > this._iVesiTilavuus) {
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!"
        } else {
            this._NykVesi += vesimaara;
            if (this._bOnkoVetta === false) {
                this._bOnkoVetta = true;
            }
            document.getElementById("result2").innerHTML = `Lisätty ${vesimaara.toFixed(2)} litraa / ${(vesimaara * 0.22).toFixed(2)} Englannin gallonaa / ${(vesimaara * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Vettä on nyt ${this._NykVesi.toFixed(2)} litraa / ${(this._NykVesi * 0.22).toFixed(2)} Englannin gallonaa / ${(this._NykVesi * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Tilaa jäljellä: ${(this._iVesiTilavuus - this._NykVesi).toFixed(2)} litraa / ${((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2)} Englannin gallonaa / ${((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2)} Amerikan gallonaa.`
            if (this._bOnkoVetta === true) {
                document.getElementById("result").innerHTML += `Vettä on tankissa`
            }
        }
    }

    PoistaVesiUSGal(vesimaara: number) {
        vesimaara *= 3.785
        if (this._NykVesi - vesimaara < 0) {
            document.getElementById("result2").innerHTML = "Lisätään liikaa vettä!"
        } else {
            this._NykVesi -= vesimaara;
            if (this._NykVesi === 0) {
                this._bOnkoVetta = false;
            }
            document.getElementById("result2").innerHTML = `Poistettu ${vesimaara.toFixed(2)} litraa / ${(vesimaara * 0.22).toFixed(2)} Englannin gallonaa / ${(vesimaara * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Vettä on nyt ${this._NykVesi.toFixed(2)} litraa / ${(this._NykVesi * 0.22).toFixed(2)} Englannin gallonaa / ${(this._NykVesi * 0.26).toFixed(2)} Amerikan gallonaa.<br/>
            Tilaa jäljellä: ${(this._iVesiTilavuus - this._NykVesi).toFixed(2)} litraa / ${((this._iVesiTilavuus - this._NykVesi) * 0.22).toFixed(2)} Englannin gallonaa / ${((this._iVesiTilavuus - this._NykVesi) * 0.26).toFixed(2)} Amerikan gallonaa.`
            if (this._bOnkoVetta === false) {
                document.getElementById("result").innerHTML += `Vettä ei ole tankissa`
            }
        }
    }
}