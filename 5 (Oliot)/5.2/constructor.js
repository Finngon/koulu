var CAjoneuvo = /** @class */ (function () {
    function CAjoneuvo() {
    }
    Object.defineProperty(CAjoneuvo.prototype, "iIskutilavuus", {
        get: function () {
            return this._iIskutilavuus;
        },
        set: function (uusiIsku) {
            this._iIskutilavuus = uusiIsku;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iHinta", {
        get: function () {
            return this._iHinta;
        },
        set: function (x) {
            this._iHinta = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iOvienLkm", {
        get: function () {
            return this._iOvienLkm;
        },
        set: function (x) {
            this._iOvienLkm = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "bKattoikkuna", {
        get: function () {
            return this._bKattoikkuna;
        },
        set: function (x) {
            this._bKattoikkuna = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iAjetutKilometrit", {
        get: function () {
            return this._iAjetutKilometrit;
        },
        set: function (x) {
            this._iAjetutKilometrit = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "sMerkki", {
        get: function () {
            return this._sMerkki;
        },
        set: function (x) {
            this._sMerkki = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iValmistusvuosi", {
        get: function () {
            return this._iValmistusvuosi;
        },
        set: function (x) {
            this._iValmistusvuosi = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "dMoottorinIskutilavuusLtr", {
        get: function () {
            return this._dMoottorinIskutilavuusLtr;
        },
        set: function (x) {
            this._dMoottorinIskutilavuusLtr = x;
        },
        enumerable: true,
        configurable: true
    });
    return CAjoneuvo;
}());
