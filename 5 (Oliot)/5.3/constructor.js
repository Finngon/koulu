var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var CAjoneuvo = /** @class */ (function () {
    function CAjoneuvo() {
    }
    Object.defineProperty(CAjoneuvo.prototype, "iIskutilavuus", {
        get: function () {
            return this._iIskutilavuus;
        },
        set: function (uusiIsku) {
            this._iIskutilavuus = uusiIsku;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iHinta", {
        get: function () {
            return this._iHinta;
        },
        set: function (x) {
            this._iHinta = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iOvienLkm", {
        get: function () {
            return this._iOvienLkm;
        },
        set: function (x) {
            this._iOvienLkm = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "bKattoikkuna", {
        get: function () {
            return this._bKattoikkuna;
        },
        set: function (x) {
            this._bKattoikkuna = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iAjetutKilometrit", {
        get: function () {
            return this._iAjetutKilometrit;
        },
        set: function (x) {
            this._iAjetutKilometrit = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "sMerkki", {
        get: function () {
            return this._sMerkki;
        },
        set: function (x) {
            this._sMerkki = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iValmistusvuosi", {
        get: function () {
            return this._iValmistusvuosi;
        },
        set: function (x) {
            this._iValmistusvuosi = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "dMoottorinIskutilavuusLtr", {
        get: function () {
            return this._dMoottorinIskutilavuusLtr;
        },
        set: function (x) {
            this._dMoottorinIskutilavuusLtr = x;
        },
        enumerable: true,
        configurable: true
    });
    return CAjoneuvo;
}());
var CHalytysAjoneuvo = /** @class */ (function (_super) {
    __extends(CHalytysAjoneuvo, _super);
    function CHalytysAjoneuvo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CHalytysAjoneuvo.prototype, "bHalytys", {
        get: function () {
            return this._bHalytys;
        },
        set: function (x) {
            this._bHalytys = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CHalytysAjoneuvo.prototype, "bRadio", {
        get: function () {
            return this._bRadio;
        },
        set: function (x) {
            this._bRadio = x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CHalytysAjoneuvo.prototype, "iKorkeus", {
        get: function () {
            return this._iKorkeus;
        },
        set: function (x) {
            this._iKorkeus = x;
        },
        enumerable: true,
        configurable: true
    });
    return CHalytysAjoneuvo;
}(CAjoneuvo));
