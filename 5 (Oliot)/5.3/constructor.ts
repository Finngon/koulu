class CAjoneuvo { 
    private _iIskutilavuus: number;
    private _iHinta: number;
    private _iOvienLkm: number;
    private _bKattoikkuna: boolean;
    private _iAjetutKilometrit: number;
    private _sMerkki: string;
    private _iValmistusvuosi: number;
    private _dMoottorinIskutilavuusLtr: number

    get iIskutilavuus(): number {
        return this._iIskutilavuus;
    }

    set iIskutilavuus(uusiIsku: number) {
        this._iIskutilavuus = uusiIsku
    }

    get iHinta(): number {
        return this._iHinta;
    }

    set iHinta(x: number) {
        this._iHinta = x
    }

    get iOvienLkm(): number {
        return this._iOvienLkm;
    }

    set iOvienLkm(x: number) {
        this._iOvienLkm = x
    }

    get bKattoikkuna(): boolean {
        return this._bKattoikkuna;
    }

    set bKattoikkuna(x: boolean) {
        this._bKattoikkuna = x
    }

    get iAjetutKilometrit(): number {
        return this._iAjetutKilometrit;
    }

    set iAjetutKilometrit(x: number) {
        this._iAjetutKilometrit = x
    }

    get sMerkki(): string {
        return this._sMerkki;
    }

    set sMerkki(x: string) {
        this._sMerkki = x
    }

    get iValmistusvuosi(): number {
        return this._iValmistusvuosi;
    }

    set iValmistusvuosi(x: number) {
        this._iValmistusvuosi = x
    }

    get dMoottorinIskutilavuusLtr(): number {
        return this._dMoottorinIskutilavuusLtr;
    }

    set dMoottorinIskutilavuusLtr(x: number) {
        this._dMoottorinIskutilavuusLtr = x
    }
}

class CHalytysAjoneuvo extends CAjoneuvo {
    private _bHalytys: boolean;
    private _bRadio: boolean;
    private _iKorkeus: number

    get bHalytys(): boolean {
        return this._bHalytys;
    }

    set bHalytys(x: boolean) {
        this._bHalytys = x
    }

    get bRadio(): boolean {
        return this._bRadio;
    }

    set bRadio(x: boolean) {
        this._bRadio = x
    }

    get iKorkeus(): number {
        return this._iKorkeus;
    }

    set iKorkeus(x: number) {
        this._iKorkeus = x
    }
}