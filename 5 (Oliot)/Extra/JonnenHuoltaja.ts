import { OstotapahtumaKuuntelijaInt } from "./OstotapahtumaKuuntelijaInt";
import { Ostotapahtuma } from "./Ostotapahtuma"

const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/gmail.readonly'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

function authorize(credentials) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        //if (err) return getNewToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
    });
    return oAuth2Client
}

export class JonnenHuoltaja implements OstotapahtumaKuuntelijaInt {
    tapahtui(lähde: Ostotapahtuma): void {
        console.log("Piiskaa!")
        // Load client secrets from a local file.
        fs.readFile('credentials.json', (err, content) => {
            if (err) return console.log('Error loading client secret file:', err);
            // Authorize a client with credentials, then call the Gmail API.
            authorize(JSON.parse(content), listLabels);
        });
        const message =
            "From: mirokles@gmail.com\r\n" +
            "To: mirokles@gmail.com\r\n" +
            "Subject: As basic as it gets\r\n\r\n" +
            "Miro Pekonen ilmoittaa, että Jonne on ostanut.";


        // The body needs to be base64url encoded.
        const base64EncodedEmail = Base64.encodeURI(message);

        gapi.client.gmail.users.messages.send({
            userId: 'me',
            resource: { // Modified
                // same response with any of these
                raw: base64EncodedEmail
                // raw: encodedMessage
                // raw: message
            }
        }).then(function () { console.log("done!") });
    }
}