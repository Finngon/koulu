export class Tuote {

    private nimi:string;
    private hinta:number;

    constructor(nimi: string, hinta:number) { 
        this.nimi = nimi;
        this.hinta = hinta; }

	public get $nimi(): string {
		return this.nimi;
	}

    public get $hinta(): number {
		return this.hinta;
	}

    public set $nimi(value: string) {
		this.nimi = value;
	}

    public set $hinta(value: number) {
		this.hinta = value;
	}

}