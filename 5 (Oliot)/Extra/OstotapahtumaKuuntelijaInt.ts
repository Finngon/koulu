import {Ostotapahtuma} from './Ostotapahtuma'
export interface OstotapahtumaKuuntelijaInt {

    tapahtui(lähde : Ostotapahtuma):void;

}