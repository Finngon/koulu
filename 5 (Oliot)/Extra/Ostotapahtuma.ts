import { Asiakas } from "./Asiakas"
import { OstotapahtumaKuuntelijaInt } from "./OstotapahtumaKuuntelijaInt"
import {OstotapahtumaKuunneltavaInt} from "./OstotapahtumaKuunneltavaInt"
import { Tuote } from "./Tuote";
export enum Tila {kesken,luotu,maksettu}

export class Ostotapahtuma implements OstotapahtumaKuunneltavaInt {

    private kuuntelijat: OstotapahtumaKuuntelijaInt[]
    private tapahtumanTila:Tila = Tila.kesken;
    private määrä: number 
    private tuote: Tuote 
    private asiakas: Asiakas 


    constructor(asiakas:Asiakas, tuote:Tuote, määrä:number) { 
        this.määrä = määrä;
        this.asiakas = asiakas;
        this.tuote = tuote;
        this.kuuntelijat = []

    }

    public lisääKuuntelija(kuuntelija:OstotapahtumaKuuntelijaInt)
    {
        // Onkohan siellä kuuntelijassa mitään? FIXME!
        this.kuuntelijat.push(kuuntelija);
    }
    public poistaKuuntelija(kuuntelija:OstotapahtumaKuuntelijaInt)
    {
        // Onkohan siellä kuuntelijassa mitään? FIXME!
        //this.kuuntelijat.push(kuuntelija);

    }
    ilmianna() {
        this.kuuntelijat.forEach(kuuntelija=>{
            kuuntelija.tapahtui(this);
        })            

    }
    /**
     * Getter $tuote
     * @return {Tuote}
     */
	public get $tuote(): Tuote {
		return this.tuote;
	}

    /**
     * Getter $ostohetki
     * @return {Date}
     */
	public get $ostohetki(): Date {
		return this.ostohetki;
	}

    /**
     * Getter $asiakas
     * @return {Asiakas}
     */
	public get $asiakas(): Asiakas {
		return this.asiakas;
	}

    /**
     * Getter $tapahtumanTila
     * @return {Tila}
     */
	public get $tapahtumanTila(): Tila {
		return this.tapahtumanTila;
	}

    /**
     * Setter $tuote
     * @param {Tuote} value
     */
	public set $tuote(value: Tuote) {
		this.tuote = value;
	}

    /**
     * Setter $ostohetki
     * @param {Date} value
     */
	public set $ostohetki(value: Date) {
		this.ostohetki = value;
	}

    /**
     * Setter $asiakas
     * @param {Asiakas} value
     */
	public set $asiakas(value: Asiakas) {
		this.asiakas = value;
	}

    /**
     * Setter $tapahtumanTila
     * @param {Tila} value
     */
	public set $tapahtumanTila(value: Tila) {
        if (value!=this.tapahtumanTila) {
            this.tapahtumanTila = value;
            this.ilmianna();
        } 
        
    }
    

    private ostohetki: Date;
    
    

}