import {OstotapahtumaKuuntelijaInt} from './OstotapahtumaKuuntelijaInt'
export interface OstotapahtumaKuunneltavaInt {

    lisääKuuntelija(kuuntelija: OstotapahtumaKuuntelijaInt):void;
    poistaKuuntelija(kuuntelija: OstotapahtumaKuuntelijaInt):void;
    ilmianna():void;
}