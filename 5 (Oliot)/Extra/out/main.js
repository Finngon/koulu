"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PerusKantaAsiakas_1 = require("./PerusKantaAsiakas");
var Tuote_1 = require("./Tuote");
var JonnenHuoltaja_1 = require("./JonnenHuoltaja");
var Ostotapahtuma_1 = require("./Ostotapahtuma");
var Asiakasryhmä;
(function (Asiakasryhmä) {
    Asiakasryhmä[Asiakasryhmä["Perus"] = 0] = "Perus";
    Asiakasryhmä[Asiakasryhmä["Premium"] = 1] = "Premium";
})(Asiakasryhmä || (Asiakasryhmä = {}));
var asiakas = new PerusKantaAsiakas_1.PerusKantaAsiakas("Pekka", "Välimaa");
var tuote = new Tuote_1.Tuote("ES Jonnen energiajuoma", 5);
var jonnenhuoltaja = new JonnenHuoltaja_1.JonnenHuoltaja();
var ostos = new Ostotapahtuma_1.Ostotapahtuma(asiakas, tuote, 4);
ostos.lisääKuuntelija(jonnenhuoltaja);
ostos.$tapahtumanTila = Ostotapahtuma_1.Tila.maksettu;
//# sourceMappingURL=main.js.map