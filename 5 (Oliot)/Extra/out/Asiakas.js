"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//import {database}
var Asiakas = /** @class */ (function () {
    function Asiakas(etunimi, sukunimi) {
        if (sukunimi === void 0) { sukunimi = ""; }
        this._etunimi = etunimi;
        this._sukunimi = sukunimi;
    }
    Object.defineProperty(Asiakas.prototype, "taso", {
        set: function (value) {
            this._taso = value;
        },
        enumerable: true,
        configurable: true
    });
    Asiakas.prototype.laskeBonus = function (x) {
        this.bonusKertymä = x * 2 * this.taso;
    };
    Asiakas.load = function (id) {
        //            return new Asiakas(database.lataa(id))
    };
    Asiakas.prototype.save = function () {
        //          db.tallenna(this._etunimi,this._sukunimi)
    };
    Object.defineProperty(Asiakas.prototype, "etunimi", {
        get: function () {
            return this._etunimi;
        },
        set: function (uusiEtunimi) {
            this._etunimi = uusiEtunimi;
        },
        enumerable: true,
        configurable: true
    });
    return Asiakas;
}());
exports.Asiakas = Asiakas;
var x = 5;
//# sourceMappingURL=Asiakas.js.map