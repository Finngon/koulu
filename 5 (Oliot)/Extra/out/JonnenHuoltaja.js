"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var googleapis_1 = require("googleapis");
var js_base64_1 = require("js-base64");
var JonnenHuoltaja = /** @class */ (function () {
    function JonnenHuoltaja() {
    }
    JonnenHuoltaja.prototype.tapahtui = function (lähde) {
        console.log("Piiskaa!");
        var message = "From: mirokles@gmail.com\r\n" +
            "To: mirokles@gmail.com\r\n" +
            "Subject: As basic as it gets\r\n\r\n" +
            "This is the plain text body of the message.  Note the blank line between the header information and the body of the message.";
        // The body needs to be base64url encoded.
        var base64EncodedEmail = js_base64_1.Base64.encodeURI(message);
        googleapis_1.gapi.client.gmail.users.messages.send({
            userId: 'me',
            resource: {
                // same response with any of these
                raw: base64EncodedEmail
                // raw: encodedMessage
                // raw: message
            }
        }).then(function () { console.log("done!"); });
    };
    return JonnenHuoltaja;
}());
exports.JonnenHuoltaja = JonnenHuoltaja;
//# sourceMappingURL=JonnenHuoltaja.js.map