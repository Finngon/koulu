"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.APP_DIR = 'src';
exports.DIST_DIR = 'dist';
exports.SOURCE_DIR = 'source';
exports.TEST_DIST = exports.DIST_DIR + "/test";
exports.OUTPUT_FILE = 'email.js';
exports.INPUT_FILE = 'index.ts';
exports.APP_NAME = 'emailjs';
//# sourceMappingURL=config.js.map