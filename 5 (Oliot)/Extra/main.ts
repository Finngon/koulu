import {PerusKantaAsiakas} from './PerusKantaAsiakas'
import { Tuote } from './Tuote'
import { JonnenHuoltaja } from './JonnenHuoltaja'
import { Ostotapahtuma, Tila } from './Ostotapahtuma'

enum Asiakasryhmä {
    Perus,
    Premium
}

let asiakas = new PerusKantaAsiakas("Pekka","Välimaa")
let tuote = new Tuote("ES Jonnen energiajuoma",5)
let jonnenhuoltaja = new JonnenHuoltaja();
let ostos = new Ostotapahtuma(asiakas, tuote, 4);
ostos.lisääKuuntelija(jonnenhuoltaja);
ostos.$tapahtumanTila = Tila.maksettu;
