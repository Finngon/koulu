- Javascript / Typescript
    - Testaukset (Mocha ja Jasmine)

- NodeJS
    - npm
    - Backend
    - Korvaisi PHP:n

- React
    - Pystyy tekemään yksinkertaisen UI:n
    - Bootstrap / JSX

- SQL
    - MongoDB voi helposti oppia


Opiskelen Full Stack-devaajaksi. Kurssi antaa minulle oppimista front endissä käyttäen Reactia, back endissä
käyttäen SQL:ää ja NodeJS:ää, sekä pilvipalvelu Azuren käyttöä. Kurssit kestää neljä kuukautta ja 
työharjoittelu kestäisi myös neljä kuukautta.

Harjoittelussa voidaan käyttää myös muita kuin kurssin opittuja tekniikoita, 
sillä kurssi antaa valmiuksia myös muiden teknologioiden ripeään omaksumiseen.