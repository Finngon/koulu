/*
Mocha is a test framework which gives its checks based on if the functions inside are true.
In order to check whatever is correct, we can use NodeJS's own function, Assert.
You can also use other test modules like Chai, which we are importing here as well.

You can install mocha by using the npm command:

npm install -g mocha

*/

var fs = require("fs"),
    assert = require("assert"),
    should = require("chai").should(),
    expect = require("chai").expect,
    mt = require("./simple_math.js")


// Describe basically gives a console log about what is inside. The
// function indents itself for easier reading.
describe('Katsotaan mt.add funktio', function () {

  // it-function is the tester. If whatever inside the function is true,
  // the function in the log checks it as complete.
  it('Pitäisi olla funktio (Assert)', function () {

    // Assert is the NodeJS's own checker. Here we are just checking if
    // mt.add is a function
    assert.ok(typeof mt.add === 'function')
  });

  //Here we do the same check but using the Chai-module
  it('Pitäisi olla funktio (Chai)', function () {

  // should be-function is from Chai module. It basically checks if the
  // variable given is that kind of variable. In this case, if mt.add
  // is a function.
    mt.add.should.be.a('function')
  });

  // Here the it-function checks if the mt.add function gives the correct
  // answer, using Assert's equal
  it('Pitäisi olla summana 15 (Assert)', function () {
    assert.equal(mt.add(10,5), 15)
  });

  // And then using Chai module's should.equal
  it('Pitäisi olla summana 15 (Chai)', function () {
    mt.add(10, 5).should.equal(15)
  });
});

describe('käydään laskutoimituksia mochalla', function () {
  before(function () {
    // Tämän describen alussa.
    console.log("Aloitetaan laskutoimituksia!");
  });
  after(function () {
    // Tämän describen lopussa.
    console.log("Kaikki laskutoimitukset käyty!");
  });
  it('Pitäisi olla funktio vähennys()', function () {
    assert.ok(typeof mt.reduce === 'function');
  });
  describe('vähennyslaskutoimituksia', function () {
    before(function () {
      // vähennyslaskutoimituksia-describen alussa.
      console.log("Seuraava laskutoimitustestiryhmä!");
    });
    after(function () {
      // vähennyslaskutoimituksia-describen lopussa.
      console.log("Laskutoimitustestiryhmä käyty!");
    });
    beforeEach(function () {
      // Ennen jokaista 'vähennyslaskutoimituksia'-describen it-toimintoa.
      console.log("Vähennyslaskua!");
    });
    afterEach(function () {
      // Jälkeen jokaisen 'vähennuslaskutoimituksia'-describen it-toiminnon.
      console.log("Valmis!");
    });
    it('pitäisi palauttaa erotuksena 5', function () {
      assert.equal(mt.reduce(11, 6), 5);
    });
    it('pitäisi palauttaa erotuksena 13', function () {
      assert.equal(mt.reduce(14, 1), 13);
    });
    it('pitäisi palauttaa erotuksena 6', function () {
      assert.equal(mt.reduce(9, 3), 6);
    });

    it.skip('pitäisi palauttaa erotuksena 96', function () {
      assert.equal(mt.reduce(100, 10), 96);
    });
  });
  it('Pitäisi olla funktio yhteensä()', function () {
    assert.ok(typeof mt.add === 'function');
  });
  describe('yhteenlaskutoimituksia', function () {
    before(function () {
      // yhteenlaskutoimituksia-describen alussa.
      console.log("Seuraava laskutoimitustestiryhmä!");
    });
    after(function () {
      // yhteenlaskutoimituksia-describen lopussa.
      console.log("Laskutoimitustestiryhmä käyty!");
    });
    beforeEach(function () {
      // Ennen jokaista 'yhteenlaskutoimituksia'-describen it-toimintoa.
      console.log("Yhteenlaskua!");
    });
    afterEach(function () {
      // Jälkeen jokaisen 'yhteenlaskutoimituksia'-describen it-toiminnon.
      console.log("Valmis!");
    });
    it('pitäisi palauttaa summana 17', function () {
      assert.equal(mt.add(11, 6), 17);
    });
    it('pitäisi palauttaa summana 13', function () {
      assert.equal(mt.add(14, 1), 15);
    });
    it('pitäisi palauttaa summana 6', function () {
      assert.equal(mt.add(9, 3), 12);
    });
    it.skip('pitäisi palauttaa summana 6', function () {
      assert.equal(mt.add(100, 10), 111);
    });
  });
  it('Pitäisi olla funktio kerto()', function () {
    assert.ok(typeof mt.times === 'function');
  });
  describe('kertolaskutoimituksia', function () {
    before(function () {
      // kertolaskutoimituksia-describen alussa.
      console.log("Seuraava laskutoimitustestiryhmä!");
    });
    after(function () {
      // kertolaskutoimituksia-describen lopussa.
      console.log("Laskutoimitustestiryhmä käyty!");
    });
    beforeEach(function () {
      // Ennen jokaista 'kertolaskutoimituksia'-describen it-toimintoa.
      console.log("Kertolaskua!");
    });
    afterEach(function () {
      // Jälkeen jokaisen 'kertolaskutoimituksia'-describen it-toiminnon.
      console.log("Valmis!");
    });
    it('pitäisi palauttaa tulona 66', function () {
      assert.equal(mt.times(11, 6), 66);
    });
    it('pitäisi palauttaa tulona 36', function () {
      assert.equal(mt.times(18, 2), 36);
    });
    it('pitäisi palauttaa tulona 27', function () {
      assert.equal(mt.times(9, 3), 27);
    });
    it.skip('pitäisi palauttaa tulona 121', function () {
      assert.equal(mt.times(11, 12), 121);
    });
  });
  it('Pitäisi olla funktio fibonacci()', function () {
    assert.ok(typeof mt.times === 'function');
  });
  describe('Fibonaccin lukujonon tutkintaa', function () {
    before(function () {
      // 'Fibonaccin lukujonon tutkintaa' -describen alussa.
      console.log("Seuraava laskutoimitustestiryhmä!");
    });
    after(function () {
      // 'Fibonaccin lukujonon tutkintaa' -describen lopussa.
      console.log("Laskutoimitustestiryhmä käyty!");
    });
    beforeEach(function () {
      // Ennen jokaista 'Fibonaccin lukujonon tutkintaa' -describen it-toimintoa.
      console.log("Fibonaccin lukujonon tutkinta!");
    });
    afterEach(function () {
      // Jälkeen jokaisen 'Fibonaccin lukujonon tutkintaa' -describen it-toiminnon.
      console.log("Tutkittu!");
    });
    it('pitäisi palauttaa luku 1', function () {
      assert.equal(mt.fibonacci(1), 1);
    });
    it('pitäisi palauttaa luku 1', function () {
      assert.equal(mt.fibonacci(3), 2);
    });
    it('pitäisi palauttaa luku 8', function () {
      assert.equal(mt.fibonacci(6), 8);
    });
    it.skip('pitäisi palauttaa luku 122', function () {
      assert.equal(mt.fibonacci(12), 122);
    });
//    it('pitäisi palauttaa luku 122', function () {
//      assert.equal(mt.fibonacci(12), 122);
//    });
  });
});

describe('Aloitetaan teksti', function(){
  it('Palautetaan teksti viiveellä...', () => {
    return mt.msg()
  });
});

describe('Aloitetaan teksti', function(){
  it('Palautetaan teksti viiveellä...', () => {
    return mt.msg()
  });
});