var fs = require("fs")

function add(a, b) {
    return a + b;
}

function reduce(a,b){
    return a-b
}

function times(a, b) {
    return a * b;
}

function fibonacci(n) {
    if (n <= 2) return 1;
    else return fibonacci(n - 1) + fibonacci(n - 2);
}

function who() {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('🤡');
        }, 200);
    });
}

function what() {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('lurks');
        }, 300);
    });
}
function where() {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('in the shadows');
        }, 500);
    });
}
async function msg() {
    const a = await who();
    const b = await what();
    const c = await where();
    console.log(`${a} ${b} ${c}`);
}

module.exports = {
    add: add,
    reduce: reduce,
    times: times,
    fibonacci: fibonacci,
    msg: msg
}