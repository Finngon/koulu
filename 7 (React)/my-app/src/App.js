import React, { useState } from 'react';
import './App.css';
import Ruutu from "./ruutu.js";

let vuorosi = true;

function App() {
  const [teksti, setTeksti] = useState("");
  const [tokatext, setToka] = useState("");

  const nappi = {
    x: "X",
    o: "O",
    none: " "
  };

  const [lauta, setLauta] = useState([nappi.none, nappi.none, nappi.none, 
                                      nappi.none, nappi.none, nappi.none, 
                                      nappi.none, nappi.none, nappi.none]);

  function Vaihda(event){
    if(event.target.value === "teksti"){
      if (teksti === "") {
        setTeksti("tekstiteksti")
      }
    }
  }

  function VaihdaToka(event){
    setTeksti(event.target.value)
  }

  function Pois(e){
    if(e.target.value !== "" && tokatext === ""){
      setToka(e.target.value + e.target.value)
    }
  }

  function VaihdaNel(event) {
    setToka(event.target.value)
  }

  function Painettu(i) {
    if (lauta[i] === nappi.none) {
      console.log(i)
        if (vuorosi) {
            lauta.splice(i,1,nappi.x)
            setLauta(lauta.slice());
            vuorosi = false;
        } else {
            lauta.splice(i,1,nappi.o)
            setLauta(lauta.slice());
            vuorosi = true;
        }
    }
}

  return (
  <div className="App">
    <header className="App-header">
      <h1>7.7a</h1>
      <input id="eka" type="text" onChange={Vaihda} />
      <input id="toka" type="text" onChange={VaihdaToka} value={teksti} />

      <h1>7.7b</h1>
      <input id="kolmas" type="text" onBlur={Pois} />
      <input id="neljäs" type="text" onChange={VaihdaNel} value={tokatext} />
      <p>a-vastausta voidaan käyttää, jos vaikka on enää yksi vaihtoehto, niin voidaan täyttää ne valmiiksi</p>
      <p>b-vastausta voidaan käyttää vaikka silloin, jos asiakas ei anna sähköpostia siihen haluamaan kenttään.</p>

      <div className="game">
          <Ruutu indeksi="0" tila={lauta[0]} painallus={Painettu} />
          <Ruutu indeksi="1" tila={lauta[1]} painallus={Painettu} />
          <Ruutu indeksi="2" tila={lauta[2]} painallus={Painettu} />
          <Ruutu indeksi="3" tila={lauta[3]} painallus={Painettu} />
          <Ruutu indeksi="4" tila={lauta[4]} painallus={Painettu} />
          <Ruutu indeksi="5" tila={lauta[5]} painallus={Painettu} />
          <Ruutu indeksi="6" tila={lauta[6]} painallus={Painettu} />
          <Ruutu indeksi="7" tila={lauta[7]} painallus={Painettu} />
          <Ruutu indeksi="8" tila={lauta[8]} painallus={Painettu} />
      </div>
    </header>
  </div>
  );
}
export default App;
