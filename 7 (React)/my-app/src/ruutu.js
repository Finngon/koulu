import React from 'react';

export function Ruutu(props){
    return(
        <button className="nappi" onClick={() => props.painallus(props.indeksi)}>
            {props.tila}
        </button>
    );
}

export default Ruutu;