// Luo 10:stä 100 000 000:aan 10 increasillä, jossa on myös otetaan aikaa, kauanko kestää.
// Käytä Aleksin tapaa 1.5-tehtävässä

// Ajan laskeminen
// Chart.js
// Kokonaisuus

var time_oi = [
    {"Kerta": 10          ,  "Aika": 0.057 },
    {"Kerta": 100         ,  "Aika": 0.052 },
    {"Kerta": 1000        ,  "Aika": 0.729 },
    {"Kerta": 10000       ,  "Aika": 7.636 },
    {"Kerta": 100000      ,  "Aika": 2.029 },
    {"Kerta": 1000000     ,  "Aika": 12.928 },
    {"Kerta": 10000000    ,  "Aika": 106.198 },
    {"Kerta": 100000000   ,  "Aika": 1048.466 },
    {"Kerta": 1000000000  ,  "Aika": 10491.418 }
]

const obj = {
    1: "maanantai",
    2: "tiistai",
    3: "keskiviikko",
    4: "torstai",
    5: "perjantai",
    6: "lauantai",
    7: "sunnuntai",
}

/* 

testaus-loop

var time_oi = [];

for (var completed = 10; completed <= Math.pow(10, 9); completed *= 10) {
    var t = performance.now();
    for (var i = 1; i <= completed; i++) {
		var tmp = Aja(Math.floor(Math.random() * 7 + 1));
	}
	time_oi.push({"Kerta": completed, "Aika": performance.now() - t});
}
console.log(time_oi)

function Aja(x){
    const obj = {
        1: "maanantai",
        2: "tiistai",
        3: "keskiviikko",
        4: "torstai",
        5: "perjantai",
        6: "lauantai",
        7: "sunnuntai",
    }

    const oi = (x) => {
        return obj[x]
    }
}

*/