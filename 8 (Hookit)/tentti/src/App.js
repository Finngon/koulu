import React, {useState, useEffect, useContext} from 'react';
import './App.css';
import 'typeface-roboto';
import { Checkbox, FormLabel, Button, ButtonGroup, Box, AppBar, Toolbar, TextField, Container, IconButton, Link, Typography } from '@material-ui/core';
import { Delete, Add, SortByAlpha, AccountCircle } from '@material-ui/icons';
import { QuizContext, QuizProvider } from './Context'
const axios = require('axios');

const instance = axios.create({baseURL: 'http://localhost:5000'})

const Question = (props) => {
  const {_, dispatch} = useContext(QuizContext)

  return(
    <div>
      {props.editing 
        ? 
        <Box>
          <TextField  id="outlined-basic" 
                      label="Question" 
                      variant="outlined" 
                      defaultValue={props.question} 
                      className="questionEdit"
                      onBlur={(event) => {
                        instance.put('/editquestion', {
                          kysymys: props.tentti[props.tenttiIndex].questions[props.qIndex].qid,
                          teksti: event.target.value
                        })
                      
                        dispatch({
                        type: 'question',
                        tenttiIndex: props.tenttiIndex,      
                        qIndex: props.qIndex,       
                        value: event.target.value
                      })}}/>
          <TextField id="outlined-basic"
                      label="Category"
                      variant="outlined"
                      defaultValue={props.qCat}
                      className="categoryEdit"
                      onBlur={(event) => {
                        /*let catemp = instance.get('/categories').then(res => res.data).catch(e => console.error(e.stack))
                        if(catemp.nimi.includes(event.target.value)){
                          instance.put('/editcategory', {
                            catid: instance.get('/categories').then(res => res.data).indexOf(event.target.value),
                            nimi: event.target.value
                          })
                         } else {
                          instance.post('/addcategory',{
                            nimi: event.target.value,
                            kysymys: props.tentti[props.tenttiIndex].questions[props.qIndex].qid
                          })
                         }*/

                      dispatch({
                        type: 'category',
                        tenttiIndex: props.tenttiIndex,      
                        qIndex: props.qIndex,       
                        category: props.qCat,
                        value: event.target.value
                      })}}/>
          <IconButton onClick={() => {
              instance.delete('/deletequestion/' + props.tentti[props.tenttiIndex].questions[props.qIndex].qid)
              
              dispatch({
                type: 'deletequestion',
                tenttiIndex: props.tenttiIndex,      
                qIndex: props.qIndex,       
              })}}><Delete /></IconButton>
        </Box>
        : 
          <h2>{props.index+1}. {props.question}</h2>
      }
      {props.answers.map((answer,i) => (
        <Box>
          {props.editing 
            ? 
            <>
              <TextField  id="outlined-basic" 
                          label="Answer" 
                          variant="outlined" 
                          defaultValue={answer.answer} 
                          onBlur={(event) => {
                            instance.put('/editanswer', {
                              vastaus: props.tentti[props.tenttiIndex].questions[props.qIndex].answers[i].aid,
                              teksti: event.target.value
                            })
 
                            dispatch({
                            type: 'answer',
                            tenttiIndex: props.tenttiIndex,      
                            qIndex: props.qIndex,       
                            aIndex: i,
                            value: event.target.value
                          })}}/>
              <IconButton onClick={() => {
                instance.delete('/deleteanswer/' + props.tentti[props.tenttiIndex].questions[props.qIndex].answers[i].aid)
              
                dispatch({
                  type: 'deleteanswer',
                  tenttiIndex: props.tenttiIndex,      
                  qIndex: props.qIndex,       
                  aIndex: i,
              })}}><Delete/></IconButton>
              <Checkbox color="primary" checked={answer.iscorrect} onClick={(event) => {
                instance.put('/editcorrect', {
                  vastaus: props.answers[i].aid,
                  oikein: event.target.checked
                })
              
                dispatch({
                  type: 'correct',
                  tenttiIndex: props.tenttiIndex,      
                  qIndex: props.qIndex,       
                  aIndex: i,
                  value: event.target.checked
                })}}></Checkbox>
              Correct answer?
            </>
            : 
              <FormLabel>
                <Checkbox checked={answer.checked} onChange={(event) =>
                dispatch({
                        type: 'box',
                        tenttiIndex: props.tenttiIndex,      
                        qIndex: props.qIndex,       
                        aIndex: i,
                        value: event.target.checked
                      })}>
                </Checkbox>
                <span className="answer">{answer.answer}</span>
                {props.showcorrect && <Checkbox color="primary" disabled={true} checked={answer.iscorrect}></Checkbox>}
              </FormLabel>
          }
        </Box>
      ))}
      {props.editing ? <Button onClick={() => {
        instance.post('/addanswer', {
          tentti: props.tenttiIndex,
          kysymys: props.qIndex
        })

        dispatch({
        type: 'addanswer',
        tenttiIndex: props.tenttiIndex,      
        qIndex: props.qIndex
      })}}><Add/>Add an answer</Button> : ""}
    </div>
  );
}

const CalculatePoints = (props) => {
  let categories = []
  let scores = [] 
  let maxpoints = []
  let total = 0
  for(let i = 0; i < props.tentit[props.tentti].questions.length; i++){
    if(!categories.includes(props.tentit[props.tentti].questions[i].category)){
      categories.push(props.tentit[props.tentti].questions[i].category)
      scores.push(0)
      maxpoints.push(0)
    }
    for(let j = 0; j < props.tentit[props.tentti].questions[i].answers.length; j++){
      if(props.tentit[props.tentti].questions[i].answers[j].iscorrect){
        total++
        maxpoints[categories.indexOf(props.tentit[props.tentti].questions[i].category)]++
        if(props.tentit[props.tentti].questions[i].answers[j].checked){
          scores[categories.indexOf(props.tentit[props.tentti].questions[i].category)]++
          console.log(scores)
        }
      }
    }
  }
  let totalscore = [...scores].reduce((a,b) => a+b,0)

  return(
    <Box>
      <h2>Pisteet:</h2>
        {categories.map((category, i) => <p>{category}: {scores[i]}/{maxpoints[i]}</p>)}
      <h2>Yhteispisteet: {totalscore}/{total}</h2>
    </Box>
  )
}

const QuizApp = () => {

  const [save, setSave] = useState(false)
  const [valittuTentti, setValittuTentti] = useState(null)
  const [tenttians, setTenttians] = useState(false)
  const [edit, setEdit] = useState(false)
  const [sort, setSort] = useState(false)
  const [login, setLogin] = useState(false)
  const [reg, setReg] = useState(false)
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [pw, setPW] = useState("")
  const [pw2, setPW2] = useState("")
  const [loggedIn,setLoggedIn] = useState(false)
  const [admin, setAdmin] = useState(false)
  const [status, setStatus] = useState("")

  //Dispatch
  const { tentit, dispatch } = useContext(QuizContext)

  useEffect(() => {
    if(!save){
      if(!localStorage.getItem("tentti")){
        localStorage.setItem("tentti", JSON.stringify(tentit))
      } else {
        Initialize()
      }
      Initialize()
      setSave(true)
    } else {
      localStorage.setItem("tentti", JSON.stringify(tentit))
    }
  },[save, tentit, dispatch])

  async function Initialize(){
    const temp = await instance.get('/tentit').then(res => res.data).catch(e => console.error(e.stack))

    for(let i = 0; i < temp.length; i++){
        temp[i].questions = await instance.get('/tentit/'+temp[i].id+'/kysymykset').then(res => res.data).catch(e => console.error(e.stack))
        for(let j = 0; j < temp[i].questions.length; j++){
            temp[i].questions[j].answers = await instance.get('/tentit/'+temp[i].id+'/kysymykset/'+temp[i].questions[j].qid+'/vastaukset').then(res => res.data).catch(e => console.error(e.stack))
        }
    }
    dispatch({type: 'initialize', data: temp})
  }

  function ShowAnswers(){  
    if (tenttians) {
      return setTenttians(false)
    } else {
      return setTenttians(true)
    } 
  }

  function Editing(){
    if(edit){
      return setEdit(false)
    } else {
      return setEdit(true)
    } 
  }

  function LogIn(){
    if(login){
      return setLogin(false)
    } else {
      return setLogin(true)
    } 
  }

  function LogOut(){
    if(window.confirm("Are you sure you want to log out?")){
      sessionStorage.removeItem("token")
      setLoggedIn(false)
      setLogin(false)
      setAdmin(false)
      setReg(false)
      setEdit(false)
      setValittuTentti(null)
      setName("")
      setEmail("")
      setPW("")
      setPW2("")
      setTenttians(false)
      setStatus("")
    }
  }

  function Register(){
    if(reg){
      return setReg(false)
    } else {
      return setReg(true)
    } 
  }

  function LoggingIn(){
    if(!email.includes("@")){
      setStatus("This is not an email. Please try again.")
    } else {
      instance.post('/auth/login',{
        email: email,
        password: pw
      }).then(function(res){
        if(res.data.auth){
          setStatus("")
          setLoggedIn(true)
          setReg(false)
          setLogin(false)
          sessionStorage.setItem("token", res.data.token)
          if(res.data.mode === "1"){
            setAdmin(true)
          }
        } else {
          setStatus("Failed to log in! Authorization failed.")
        }
      }).catch(function(err){
        setStatus("Password or username does not match!")
      })
    }
  }

  function RegisterPerson(){
    if(name == "" || email == "" || pw == "" || pw2 == ""){
      setStatus("All fields are mandatory. Please try again.")
    } else if(pw !== pw2){
      setStatus("Failed to confirm password. Please try again")
    } else if(!email.includes("@")){
      setStatus("This is not an email. Please try again.")
    } else {
      instance.post('/auth/register', {
        nimi: name,
        email: email,
        pw: pw
      }).then(function(res){
        if(res.data.auth){
          setStatus("")
          setLoggedIn(true)
          setReg(false)
          setLogin(false)
          sessionStorage.setItem("token", res.data.token)
        }
      }).catch(function (err){
        setStatus(err.response.data.message)
      })
    }
  }
  
  function SortThis(){
    if(sort){
      let changeSort = setSort(false)
      let sortIt = dispatch({
        type: 'sort',
        order: false,
        tenttiIndex: valittuTentti,
        swapTentti: setValittuTentti
      })
      return changeSort && sortIt
    } else {
      let changeSort = setSort(true)
      let sortIt = dispatch({
        type: 'sort',
        order: true,
        tenttiIndex: valittuTentti,
        swapTentti: setValittuTentti
      })
      return changeSort && sortIt
    }
  }

  function AddQuiz(){
    let tenttiNimi = window.prompt("Anna uuden tentin nimi")

    instance.post('/addquiz', {
      name: tenttiNimi,
    })

    return dispatch({
      type: 'addquiz',
      name: tenttiNimi
    })
  }

  return (
  <>
    <AppBar position="sticky">
      <Toolbar>
        {loggedIn && <ButtonGroup color="inherit" variant="text" style={{flex: 1, flexdirection: 'row', alignItems: 'flex-start'}}>
          {tentit.map((tentti, i) => <Button
            onClick={() => setValittuTentti(i)}>{tentti.name}</Button>
          )}
          <Button onClick={SortThis}><SortByAlpha/></Button>
          {loggedIn && admin && <Button color="inherit" onClick={Editing}>{edit ? "Finish editting" : "Edit Answers"}</Button>}
        </ButtonGroup>}
        {loggedIn ? 
          <IconButton style={{ alignSelf: 'flex-end', margin: 'auto', color: 'white'}} onClick={LogOut}>{"Log out "}<AccountCircle color= "inherit"/></IconButton>
          : <IconButton style={{ alignSelf: 'flex-end', margin: 'auto', color: 'white'}} onClick={LogIn}>{" Log in "}<AccountCircle color= "inherit"/></IconButton>}
      </Toolbar>
    </AppBar>
      {valittuTentti !==null && !login && tentit[valittuTentti].questions.map((question,i) =>
      <Container maxWidth="md" className="question" style={{ backgroundColor: "WhiteSmoke" }}>
        <Question
          key={i}
          index={i} 
          qIndex={i} 
          editing={edit} 
          qCat={question.category} 
          showcorrect={tenttians}
          tentti={tentit}
          tenttiIndex={valittuTentti} 
          {...question}/>
      </Container>
      )}
      { admin && <Container maxWidth="md" className="answers" style={{backgroundColor: "LightGray" }}>
        {valittuTentti !== null ? 
          <Button color="primary" onClick={() => {
            instance.post('/addquestion', {
              tentti: tentit[valittuTentti].id
            })

            dispatch({
            type: 'addquestion',
            tenttiIndex: valittuTentti,
          })}}>
          <Add/>Add a question</Button> 
        : ""}
        {valittuTentti !== null && !edit && <Button onClick={ShowAnswers} color="primary">{tenttians ? "Hide Answers" : "Show Answers"}</Button> }
        {valittuTentti !== null && !edit && tenttians && <CalculatePoints tentit={tentit} tentti={valittuTentti}/>}
        {edit && !tenttians && <Button color="primary" onClick={AddQuiz}><Add/>Add a Quiz</Button>}
        {valittuTentti !== null && edit && !tenttians && <Button color="primary" onClick={() => {
          if(window.confirm("Are you sure you want to delete this quiz?")){
            instance.delete('/deletequiz/' + tentit[valittuTentti].id)

            dispatch({
              type: 'deletequiz',
              tenttiIndex: valittuTentti,
              swapTentti: setValittuTentti
            })}
          }}><Delete/>Delete a Quiz</Button>}
      </Container> }
      
      
      {login && !reg && <Container maxWidth='sm' style={{
          paddingTop: '2vh'
      }}>
        <TextField  id="outlined-basic" 
                    label="Sähköposti" 
                    variant="outlined"
                    onChange={e => setEmail(e.target.value)}>               
        </TextField><br/>
        <TextField
          id="standard-password-input"
          label="Password"
          variant="outlined"
          type="password"
          autoComplete="current-password"
          onChange={e => setPW(e.target.value)}
        /><br/>
        <Typography color="secondary">{status}</Typography>
        <Button variant="contained" color="primary" onClick={LoggingIn} >Log In</Button><br/>
      {login && !reg && <Typography><Link href="#" onClick={Register}>{"No account? Register here"}</Link></Typography>}                    
      </Container>}


      {login && reg && <Container maxWidth='sm' style={{
          paddingTop: '2vh'
      }}>
        <TextField  id="outlined-basic" 
                    label="Nimi" 
                    variant="outlined"
                    onChange={e => setName(e.target.value)}>               
        </TextField><br/>
        <TextField  id="outlined-basic" 
                    label="Sähköposti" 
                    variant="outlined"
                    onChange={e => setEmail(e.target.value)}>
        </TextField><br/>
        <TextField
          id="standard-password-input"
          label="Password"
          variant="outlined"
          type="password"
          autoComplete="current-password"
          onChange={e => setPW(e.target.value)}
        /><br/>
        <TextField
          id="standard-password-input"
          label="Retype Password"
          variant="outlined"
          type="password"
          autoComplete="current-password"
          onChange={e => setPW2(e.target.value)}
        /><br/>
        <Typography color="secondary">{status}</Typography>
        <Button variant="contained" color="primary" onClick={RegisterPerson}>Register</Button><br/>
      {login && reg && <Typography><Link href="#" onClick={Register}>{"Have an account? Log in here"}</Link></Typography>}                    
      </Container>}
   </>
  )
}

const App = () => {
  return(
    <QuizProvider>
      <QuizApp/>
    </QuizProvider>
  )
}


export default App