import React, { useReducer } from "react";

let initialState = [];

let reducer = (state, action) => {
    let newState = JSON.parse(JSON.stringify(state))
    switch (action.type) {
        case 'box':
            newState[action.tenttiIndex].questions[action.qIndex].answers[action.aIndex].checked = action.value
            return newState;
        case 'correct':
            newState[action.tenttiIndex].questions[action.qIndex].answers[action.aIndex].iscorrect = action.value
            return newState;
        case 'category':
            newState[action.tenttiIndex].questions[action.qIndex].category = action.value;
            return newState;
        case 'answer':
            newState[action.tenttiIndex].questions[action.qIndex].answers[action.aIndex].answer = action.value;
            return newState;
        case 'question':
            newState[action.tenttiIndex].questions[action.qIndex].question = action.value;
            return newState;
        case 'addanswer':
            newState[action.tenttiIndex].questions[action.qIndex].answers.push(
                {
                    answer: "",
                    checked: false,
                    iscorrect: false
                }
            )
            return newState;
        case 'addquestion':
            newState[action.tenttiIndex].questions.push(
                {
                    category: "",
                    question: "",
                    answers: []
                }
            );
            return newState;
        case 'addquiz':
            newState.push(
                {
                    name: action.name,
                    questions: []
                }
            );
            return newState;
        case 'deleteanswer':
            newState[action.tenttiIndex].questions[action.qIndex].answers.splice(action.aIndex, 1)
            return newState;
        case 'deletequestion':
            newState[action.tenttiIndex].questions.splice(action.qIndex, 1)
            return newState;
        case 'deletequiz':
            if(action.tenttiIndex === newState.length-1){
                action.swapTentti(action.tenttiIndex-1)
            }
            newState.splice(action.tenttiIndex, 1)
            return newState;
        case 'initialize':
            initialState = action.data
            return initialState;
//        case 'reset':
//            newState = initialState
//            return newState;
        case 'sort':
            if(action.tenttiIndex !== null){
                let temp = newState[action.tenttiIndex].name
                if(action.order){
                    newState.sort((a, b) => a.name.localeCompare(b.name, 'fi', { ignorePunctuation: true }))
                } else {
                    newState.sort((a, b) => b.name.localeCompare(a.name, 'fi', { ignorePunctuation: true }))
                }
                action.swapTentti(newState.findIndex(name => name.name === temp))
            } else if(action.order) {
                newState.sort((a, b) => a.name.localeCompare(b.name, 'fi', { ignorePunctuation: true }))
            } else {
                newState.sort((a, b) => b.name.localeCompare(a.name, 'fi', { ignorePunctuation: true }))
            }
            return newState;
        default:
            throw new Error();
    }
}

 
/*[
        {
            name: "Matikka",
            id: 1,
            questions: [
                {
                    category: "Simple Math",
                    question: "1+2?",
                    q_id: 1,
                    answers: [
                        {
                            answer: "3",
                            checked: false,
                            iscorrect: true,
                            a_id: 1
                        },
                        {
                            answer: "4",
                            checked: false,
                            iscorrect: false,
                            a_id: 2
                        }
                    ]
                },
                {
                    category: "Simple Math",
                    question: "9^2?",
                    q_id: 2,
                    answers: [
                        {
                            answer: "18",
                            checked: false,
                            iscorrect: false,
                            a_id: 3
                        },
                        {
                            answer: "81",
                            checked: false,
                            iscorrect: true,
                            a_id: 4
                        }
                    ]
                }
            ]
        },
        {
            name: "Fysiikka",
            id: 2,
            questions: [
                {
                    category: "Simple Physics",
                    question: "Painovoiman suuruus Maassa?",
                    q_id: 3,
                    answers: [
                        {
                            answer: "9,81 m/s^2",
                            checked: false,
                            iscorrect: true,
                            a_id: 5
                        },
                        {
                            answer: "8,91 m/s^2",
                            checked: false,
                            iscorrect: false,
                            a_id: 6
                        }
                    ]
                },
                {
                    category: "Simple Physics",
                    question: "Kuka löysi painovoiman",
                    q_id: 4,
                    answers: [
                        {
                            answer: "Albert Einstein",
                            checked: false,
                            iscorrect: false,
                            a_id: 7
                        },
                        {
                            answer: "Isaac Newton",
                            checked: false,
                            iscorrect: true,
                            a_id: 8
                        }
                    ]
                }
            ]
        },
    ];
*/

const QuizContext = React.createContext(initialState);

function QuizProvider(props) {
    const [tentit, dispatch] = useReducer(reducer, initialState);

    return(
        <QuizContext.Provider value={{tentit, dispatch}}>
            {props.children}
        </QuizContext.Provider>
    )
}

export { QuizContext, QuizProvider };