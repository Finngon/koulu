import React, {useState} from 'react';

function Assignment13a() {
  const [txtout, setOut] = useState('');
  
  function testText(event) {
    if (event.target.value === 'teksti' && txtout === '') setOut('tekstiteksti')
  }

  return (
    <div>
      <input type="text" id="in" onChange={testText} /><br/>
      <input type="text" id="in" value={txtout} onChange={(vnt) => {setOut(vnt.target.value)}} />
    </div>
  );
}

function Assignment13b() {
  const [txtout, setOut] = useState('');

  function testText(event) {
    if (txtout === '') setOut(event.target.value + event.target.value)
  }

  return (
    <div>
      <input type="text" id="in" onBlur={testText} /><br/>
      <input type="text" id="in" value={txtout} onChange={(vnt) => {setOut(vnt.target.value)}} />
    </div>
  );
}

function App() {
  return (
    <div className="App">
      <Assignment13a />
      <hr/>
      <Assignment13b />
    </div>
  );
}

export default App;
