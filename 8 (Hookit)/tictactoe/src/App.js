import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const [count, setCount] = useState(0);
  const [text, setText] = useState('');

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Count is: {count}<br/>Text is: {text}</p>
        <input type="text" id="txt" value={text} onChange={(vnt) => setText(vnt.target.value)} onMouseOver={() => setCount(count+1)} />
      </header>
    </div>
  );
}

export default App;
