import React, {useState} from 'react';

// The bomb
function Bomb(props) {
  // All bomb parts use the same style
  const bombStyle = { fill: '#222', strokeWidth: 10, strokeLinecap: 'round', stroke: '#222' };

  return (<g>
    <line x1="25" y1="25" x2="75" y2="75" style={bombStyle} />
    <line x1="25" y1="75" x2="75" y2="25" style={bombStyle} />
    <line x1="50" y1="10" x2="50" y2="90" style={bombStyle} />
    <line y1="50" x1="10" y2="50" x2="90" style={bombStyle} />
    <circle cx="50" cy="50" r="25" style={bombStyle} />
    <circle cx="37" cy="42" r="8" style={{fill:'#fff'}} />
  </g>);
}

// The fiels region aka cell
function Cell(props) {
  const colors = ['none', '#00b', '#0b0', '#b00', '#006', '#600', '#088', '#222', '#888'];

  // Styles for SVG cell elements
  const buttonStyle   = { width: props.size + 'pt', height: props.size + 'pt',
    boxSizing: 'border-box', margin: '0pt' };
  const openStyle     = { border: 'solid', borderWidth: (props.size / 40) + 'pt',
    borderColor: '#888', background: '#ccc' };
  const closedStyle   = { border: 'solid', borderWidth: (props.size / 12) + 'pt',
    borderColor: '#ccc #888 #666 #aaa', background: '#ddd' };
  const textStyle     = { fontSize: '80', fontWeight: 'bold', textAlign: 'center',
    verticalAlign: 'center', fill: colors[props.relation > 0 ? props.relation : 0] };
  const flagLineStyle = {
    stroke: '#222', strokeWidth: '10' };
  const flagStyle     = {
    fill: '#d00', stroke: '#d00' };

  // Open cell with bomb
  if (props.open && props.relation === -1)
    return (<svg viewBox="0 0 100 100" style={{ ...buttonStyle, ...openStyle }}>
      <Bomb />
    </svg>);
  // Open empty cell
  if (props.open)
    return (<svg viewBox="0 0 100 100" style={{ ...buttonStyle, ...openStyle }}>
      <text x="25" y="80" style={textStyle}>{props.relation}</text>
    </svg>);
  // Closed cell with flag icon
  if (props.flag === 1)
    return (<svg viewBox="0 0 100 100" onContextMenu={props.flagAction} style={{ ...buttonStyle, ...closedStyle }}>
      <line x1="35" x2="75" y1="85" y2="85" style={flagLineStyle} />
      <line x1="55" x2="55" y1="40" y2="85" style={flagLineStyle} />
      <polygon points="60,50 60,15 20,33" style={flagStyle} />}
    </svg>);
  // Closed cell with question mark
  if (props.flag === 2)
    return (<svg viewBox="0 0 100 100" onClick={props.action} onContextMenu={props.flagAction} style={{ ...buttonStyle, ...closedStyle }}>
      <text x="25" y="80" style={{...textStyle, fill: '#222'}}>?</text>
    </svg>);
  // Closed cell with bomb
  if (props.flag === -1)
    return (<svg viewBox="0 0 100 100" onClick={props.action} onContextMenu={props.flagAction} style={{ ...buttonStyle, ...closedStyle }}>
      <Bomb key={props.key + 'bomb'} />
    </svg>);
  // closed cell without any tags
  return (<svg viewBox="0 0 100 100" onClick={props.action} onContextMenu={props.flagAction} style={{ ...buttonStyle, ...closedStyle }} />);
}

function createField(width, height, bombs) {
  const around = [[-1,-1],[0,-1],[1,-1],[-1,0],[1,0],[-1,1],[0,1],[1,1]];
  const field = Array.from({length:height}, () => Array.from({length:width}, () => [false, 0, 0]));

  const tmp = new Set();
  while (tmp.size < bombs)
    tmp.add(parseInt(Math.random() * width) + width * parseInt(Math.random() * height));
  const locations = [...tmp].map((n) => [parseInt(n % width), parseInt(n / width)]);

  locations.forEach(([x, y]) => around.forEach(([xx,yy]) => field[yy+y] && field[yy+y][xx+x] && field[yy+y][xx+x][1]++));
  locations.forEach(([x, y]) => field[y][x][1] = -1);

  return field;
}

export default function Minesweeper(props) {
//  const [width, height, bombs, cellSize] = [20, 15, 30, 30]; // *Medium
//  const [width, height, bombs, cellSize] = [25, 17, 80, 30]; // *Hard
  const [width, height, bombs, cellSize] = [5, 4, 2, 100]; // Quick
//  const [width, height, bombs, cellSize] = [40, 25, 10, 20]; // Automatic
//  const [width, height, bombs, cellSize] = [ 8,  8, 10, 30]; // Beginner
//  const [width, height, bombs, cellSize] = [30, 16, 99, 30]; // Hard
  const [field,  setField]  = useState(createField(width, height, bombs));
  const [gameOn, setGameOn] = useState(true);

  function checkField(x, y, event, newField=null) {
    const around = [[-1,-1],[0,-1],[1,-1],[-1,0],[1,0],[-1,1],[0,1],[1,1]];
    if (!newField) newField = JSON.parse(JSON.stringify(field));
    if (x < 0 || y < 0 || x >= width || y >= height || newField[y][x][0]) return;
    if (field[y][x][1] < 0) {
      newField = newField.map((arr) => arr.map(([op, rel, fl]) => [op, rel, rel === -1 ? -1 : op]));
      setGameOn(false);
    } else {
      newField[y][x][0] = true;
      if (field[y][x][1] === 0) around.forEach(([xx,yy]) => checkField(xx+x, yy+y, null, newField));
    }

    let freeUnknown = 0;
    newField.forEach((row) => row.forEach(function ([op, rel, fl]) { if (rel >= 0 && !op) freeUnknown++ }));
    if (freeUnknown === 0) {
      newField = newField.map((row) => row.map(([op, rel, fl]) => [true, rel, fl]));
      setGameOn(false);
    }
    setField(newField);
  }

  function flagField(x, y, event) {
    event.preventDefault();
    const newField = JSON.parse(JSON.stringify(field));
    newField[y][x][2] = (newField[y][x][2] + 1) % 3;
    setField(newField);
  }

  const divStyle = {margin: '0pt', border: 'solid #444 0px', padding: '0pt'};
  return (<div key="minefield" style={{...divStyle}}>{
    field.map((row, y) => <div key={'minefieldRow' + y} style={{...divStyle, height: cellSize + 'pt'}}> {
      row.map(([op, rel, fl], x) => <Cell key={'cell_' + x + '_' + y}
        size={cellSize} open={op} relation={rel} flag={fl}
        action={gameOn ? checkField.bind(null, x, y) : undefined}
        flagAction={gameOn ? flagField.bind(null, x, y) : undefined} />)
    }</div>)}</div>);
}