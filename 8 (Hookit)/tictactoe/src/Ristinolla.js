import './Ristinolla.css'
import React, {useState, useEffect} from 'react';
const W3CWebSocketClient = require('websocket').w3cwebsocket;

function openConnection(callback) {
//  // https://www.websocket.in/
//  const channel = '0';
//  const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjRhNmM0OGY4MDE4MGE5NGYzM2ZmZmZhYzM0N2IzNzhmMDllYmM4ZjQ1ZGZjYzgyYzRkY2ExZDFiZDk4ZGM5ZmM1YmIxZjMwYTgyODkyOTkwIn0.eyJhdWQiOiI2IiwianRpIjoiNGE2YzQ4ZjgwMTgwYTk0ZjMzZmZmZmFjMzQ3YjM3OGYwOWViYzhmNDVkZmNjODJjNGRjYTFkMWJkOThkYzlmYzViYjFmMzBhODI4OTI5OTAiLCJpYXQiOjE1NzQ4NDI0ODUsIm5iZiI6MTU3NDg0MjQ4NSwiZXhwIjoxNjA2NDY0ODg1LCJzdWIiOiI3OSIsInNjb3BlcyI6W119.BcO5VxY5YmuWwnEHA9nYMvY0lz-SsaHzr_EeBL0ZhP3qd0MRfJ6d92l8sAcp2baWYDFJLqubtEBk9MrSr3Sz5ardb66Na1VdsdpzlEPuoh9syqvTe-6mhhSV52fJrxeJJLmFUO-yrDBt74ofoZSt44HxMQ3SweG-46vc1UmT1LpJVB-RBZWkFk57Vpu192nSYeOfe-vuPohnps6XxD_in5P_33DpJAF6JK2ISSFySPIcR3OS-0_v8S70k1K8BB_rg1yYSDNNNrFcre3H5_69n_jRrWkhqqzb3kFT5v48q2WtSRge4IbDC7K0-b8kQ4BJWG-IbLDewCfPUwrKVuDxGH7ATFQfLyy5pFFWGo8JYZdZNS-y11x9EiYTlv7c_NVKmMZWlRDK52ArwTcld28UKU6_3N-MN5R3bOEID1d5QTip-ntozcmWevShBG77rTDdHK1W00VCAXH_O8tMLHtUvdf231r2h7QwuFQAg6O9xgxH4U7Fc8AIsL7MrprDIp2srn1L7M8GgtglkAMsUyTMYqMw2YivxBkop3WHZNaAKBfxZ-9Vib4HJnpVVGs9IwjqMtuVLPoDZL24WmsaeDW_3aRiqhNSVAuWJbZhGA5MDvp3vlttsaIZpgK0hdzi2YeHDkvmsQKgu1h--pUTKfADPCAtD79OZOt-6u0-i32jgno';
//  const url = `wss://connect.websocket.in/v2/{channel}?token={token}`;
  const url = 'ws:seitz.fi:8765/';

  const client = new W3CWebSocketClient(url);
  client.onerror = function() { console.log('Connection failed'); }
  client.onclose = function() { console.log('Connection closed'); };
  client.onopen  = function() {
    console.log('Connection opened');
    if (client.readyState === client.OPEN) client.send('         ');
  };
  client.onmessage = callback;
  return client;
}

function checkWinner(board) {
  const LINES = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]];
  return LINES.reduce((set, ls) => new Set(['xxx','ooo']).has(ls.map((i) => board[i]).join('')) ? new Set([...set, ...ls]) : set, new Set());
}

function Rotating() {
  return (<svg className="thinking" viewBox="0 0 100 100"><g>
    <animateTransform attributeName="transform" type="rotate" from="0 50 50" to="360 50 50" dur="5s" repeatCount="indefinite" />
    <circle cx="50" cy="50" r="35" />
    <polygon points="50,3 50,27 65,15" />
    <polygon points="50,97 50,72 35,85" />
  </g></svg>);
}

function Cell(props) {
  return (
    <svg className={"cell " + props.value + (props.hilight ? ' hilight' : '')} onClick={props.value === ' ' ? props.action : undefined}>
      <circle r="40%" cx="50%" cy="50%" />
      <line x1="10%" y1="10%" x2="90%" y2="90%" />
      <line x1="10%" y1="90%" x2="90%" y2="10%" />
    </svg>);
}

function Board() {
  // Sets the states needed. 
  const connect = true;
  const aiType = 1;
  const [board, setBoard] = useState([]);
  const [human, setHuman] = useState(true);
  const [thinking, setThinking] = useState(false);
  const [winner, setWinner] = useState(new Set());
  const [client, setClient] = useState(null);

  if (client === null && connect) setClient(openConnection(function (message) {
    console.log(message.data);
    setHuman(true);
    if (message.data === '         ') {
      newGame(false);
      setHuman(true);
    } else {
      const newBoard = message.data.split('').map((xo) => xo === ' ' ? ' ' : (xo === 'x' ? 'o' : 'x'));
      setBoard(newBoard);
      setWinner(checkWinner(newBoard));
    }
  }));

  // Create boolean of game being active
  // 1. Check that there is a board
  // 2. Check that there is no winner
  // 3. Check that there are cells available
  const gameon = board.length > 0 && winner.size === 0 && new Set(board).has(' ');

  function newGame(send=true) {
    setBoard(Array(9).fill(' '));
    setWinner(new Set());
    setThinking(false);
    setHuman(!connect);
    if (send && client && client.readyState === client.OPEN) client.send('         ');
  }

  function cellClicked(index, event) {
    const newBoard = [...board.slice(0, index), human ? 'x' : 'o', ...board.slice(index + 1)];
    setWinner(checkWinner(newBoard));
    setBoard(newBoard);
    setHuman(!human);
    if (client !== null) client.send(newBoard.join(''));
  }

  function minimax(board, node, depth=9, cpu=true) {
    // If the bottom is reached, return zero
    if (depth === 0) return 0;
    // Don't test turns for occupied cells
    if (board[node] !== ' ') return 0;
    // Generate new board
    let newBoard = [...board.slice(0, node), cpu ? 'o' : 'x', ...board.slice(node + 1)];
    // Check if game has been won
    if (checkWinner(newBoard).size > 0) return (cpu ? 1 : -1) * depth;

    // Run thru all possible next turns
    if (!cpu) {
      // From computer turns select maximum value for turn
      let value = -Infinity;
      newBoard.forEach((symbol, child) => value = Math.max(minimax(newBoard, child, depth - 1, !cpu), value));
      return value;
    } else {
      // And from emey turns select minimum possible
      let value = Infinity;
      newBoard.forEach((symbol, child) => value = Math.min(minimax(newBoard, child, depth - 1, !cpu), value));
      return value;
    }
  }

  function minimaxAI() {
    // Check that game is on
    if (!gameon) return 0;
    console.log('Starting to think');
    // Create a map of possible turns by their "win value"
    const turns = {};
    board.forEach((xo, n) => { if (board[n] === ' ') turns[minimax(board, n)] = n; });
    let select = turns[Object.keys(turns).sort().reverse()[0]];
    console.log('Decision table: ' + JSON.stringify(turns) + ', Selection: ' + select);
    // And select largest value
    cellClicked(select, null);
  }

  function randomAI() {
    let r = 0;
    do { r = parseInt(Math.random() * board.length) } while (board[r] !== ' ');
    cellClicked(r, null);
  }

  async function calculateTurn() {
    if (client === null) {
      setThinking(true);
      // Timeout to display the fancy waiting animation
      await new Promise(resolve => setTimeout(resolve, 500));
      // Animation doesn't roll when computer is thinking. Maybe the process is too heavy
      switch (aiType) {
        case 1: minimaxAI(); break
        default: randomAI(); break }
      setThinking(false);
      setHuman(true);
    }
  };

  // Game will check that if the game is on and it's human's turn, it'll count the click.
  if (gameon && !thinking && !human) calculateTurn();

  if (!board.length) newGame();

  return (<div className="App">
    <div className="board">
      {board.map((val, i) => (<Cell value={val} key={'cell_' + i} hilight={winner.has(i)} action={(gameon && human) ? cellClicked.bind(undefined, i) : undefined } />))}
      <button className="newgame" onClick={newGame}>New game</button><br/>
    </div>
    { (!human && gameon) ? <Rotating key="waiting" /> : '' }
  </div>);
}

export default Board;
