import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import axios from 'axios';
import './App.css';

function App() {

  const [apiResponse, setResponse] = useState("");
  const [nimi, setNimi] = useState("Miro");
  const url = "http://localhost:5000/";

  useEffect(() => {
    axios.post(url, {
      name: nimi
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  });

  function Muuta(event){
    setNimi(event.target.value);
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        <input type="text" value={nimi} onChange={Muuta}/>

        <p className="App-intro">{apiResponse}</p>
      </header>
    </div>
  );
}

export default App;
