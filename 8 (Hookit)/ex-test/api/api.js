var express = require("express");
const app = express()
var router = express.Router();
const port = 5000
var cors = require('cors')

app.use(cors())
app.use(express.json())

router.get("/", function(req, res, next) {
    res.send("API is working properly");
});

app.post('/', (req, res, next) => {
    console.log(req.body.name)
    res.json(req.body)
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))