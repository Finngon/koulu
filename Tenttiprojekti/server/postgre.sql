PGDMP     !    :    	    
        x            postgres    9.6.16    9.6.16 a    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    12401    postgres    DATABASE     �   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Finnish_Finland.1252' LC_CTYPE = 'Finnish_Finland.1252';
    DROP DATABASE postgres;
             postgres    false            �           0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                  postgres    false    2238                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    4                        3079    12387    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    2                        3079    16384 	   adminpack 	   EXTENSION     A   CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
    DROP EXTENSION adminpack;
                  false            �           0    0    EXTENSION adminpack    COMMENT     M   COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';
                       false    1            �            1259    16519 	   Kategoria    TABLE     q   CREATE TABLE public."Kategoria" (
    kategoria_id bigint NOT NULL,
    kategoria_nimi character varying(128)
);
    DROP TABLE public."Kategoria";
       public         postgres    false    4            �            1259    16517    Kategoria_kategoria_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Kategoria_kategoria_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public."Kategoria_kategoria_id_seq";
       public       postgres    false    207    4            �           0    0    Kategoria_kategoria_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public."Kategoria_kategoria_id_seq" OWNED BY public."Kategoria".kategoria_id;
            public       postgres    false    206            �            1259    16484    Kysymys    TABLE     �   CREATE TABLE public."Kysymys" (
    kysymys_id bigint NOT NULL,
    tentti_id bigint NOT NULL,
    kategoria_id bigint NOT NULL,
    kysymys_teksti character varying(1000) NOT NULL
);
    DROP TABLE public."Kysymys";
       public         postgres    false    4            �            1259    16482    Kysymys_kategoria_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Kysymys_kategoria_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public."Kysymys_kategoria_id_seq";
       public       postgres    false    202    4            �           0    0    Kysymys_kategoria_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public."Kysymys_kategoria_id_seq" OWNED BY public."Kysymys".kategoria_id;
            public       postgres    false    201            �            1259    16478    Kysymys_kysymys_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Kysymys_kysymys_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public."Kysymys_kysymys_id_seq";
       public       postgres    false    202    4            �           0    0    Kysymys_kysymys_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public."Kysymys_kysymys_id_seq" OWNED BY public."Kysymys".kysymys_id;
            public       postgres    false    199            �            1259    16480    Kysymys_tentti_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Kysymys_tentti_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public."Kysymys_tentti_id_seq";
       public       postgres    false    202    4            �           0    0    Kysymys_tentti_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public."Kysymys_tentti_id_seq" OWNED BY public."Kysymys".tentti_id;
            public       postgres    false    200            �            1259    16421    Käyttäjä    TABLE     
  CREATE TABLE public."Käyttäjä" (
    kayttaja_id bigint NOT NULL,
    kayttaja_nimi character varying(128) NOT NULL,
    kayttaja_salasana character varying(255) NOT NULL,
    kayttaja_tyyppi bigint NOT NULL,
    kayttaja_email character varying(128) NOT NULL
);
 !   DROP TABLE public."Käyttäjä";
       public         postgres    false    4            �            1259    16450    Käyttäjä-Ryhmä    TABLE     l   CREATE TABLE public."Käyttäjä-Ryhmä" (
    kayttaja_id bigint NOT NULL,
    ryhma_id bigint NOT NULL
);
 (   DROP TABLE public."Käyttäjä-Ryhmä";
       public         postgres    false    4            �            1259    16446 %   Käyttäjä-Ryhmä_käyttäjä_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Käyttäjä-Ryhmä_käyttäjä_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public."Käyttäjä-Ryhmä_käyttäjä_id_seq";
       public       postgres    false    4    196            �           0    0 %   Käyttäjä-Ryhmä_käyttäjä_id_seq    SEQUENCE OWNED BY     p   ALTER SEQUENCE public."Käyttäjä-Ryhmä_käyttäjä_id_seq" OWNED BY public."Käyttäjä-Ryhmä".kayttaja_id;
            public       postgres    false    194            �            1259    16448     Käyttäjä-Ryhmä_ryhmä_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Käyttäjä-Ryhmä_ryhmä_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public."Käyttäjä-Ryhmä_ryhmä_id_seq";
       public       postgres    false    4    196            �           0    0     Käyttäjä-Ryhmä_ryhmä_id_seq    SEQUENCE OWNED BY     h   ALTER SEQUENCE public."Käyttäjä-Ryhmä_ryhmä_id_seq" OWNED BY public."Käyttäjä-Ryhmä".ryhma_id;
            public       postgres    false    195            �            1259    16417    Käyttäjä_käyttäjä_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Käyttäjä_käyttäjä_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public."Käyttäjä_käyttäjä_id_seq";
       public       postgres    false    193    4            �           0    0    Käyttäjä_käyttäjä_id_seq    SEQUENCE OWNED BY     b   ALTER SEQUENCE public."Käyttäjä_käyttäjä_id_seq" OWNED BY public."Käyttäjä".kayttaja_id;
            public       postgres    false    191            �            1259    16419 "   Käyttäjä_käyttäjä_tyyppi_seq    SEQUENCE     �   CREATE SEQUENCE public."Käyttäjä_käyttäjä_tyyppi_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public."Käyttäjä_käyttäjä_tyyppi_seq";
       public       postgres    false    4    193            �           0    0 "   Käyttäjä_käyttäjä_tyyppi_seq    SEQUENCE OWNED BY     j   ALTER SEQUENCE public."Käyttäjä_käyttäjä_tyyppi_seq" OWNED BY public."Käyttäjä".kayttaja_tyyppi;
            public       postgres    false    192            �            1259    16467    Käyttäjätyyppi    TABLE     �   CREATE TABLE public."Käyttäjätyyppi" (
    kayttajatyyppi_id bigint NOT NULL,
    kayttajatyyppi_nimi character varying(128)
);
 '   DROP TABLE public."Käyttäjätyyppi";
       public         postgres    false    4            �            1259    16465 '   Käyttäjätyyppi_kayttajatyyppi_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Käyttäjätyyppi_kayttajatyyppi_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE public."Käyttäjätyyppi_kayttajatyyppi_id_seq";
       public       postgres    false    4    198            �           0    0 '   Käyttäjätyyppi_kayttajatyyppi_id_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public."Käyttäjätyyppi_kayttajatyyppi_id_seq" OWNED BY public."Käyttäjätyyppi".kayttajatyyppi_id;
            public       postgres    false    197            �            1259    16406    Ryhmä    TABLE     f   CREATE TABLE public."Ryhmä" (
    ryhma_id bigint NOT NULL,
    ryhma_nimi character varying(128)
);
    DROP TABLE public."Ryhmä";
       public         postgres    false    4            �            1259    16404    Ryhmä_ryhmä_id_seq    SEQUENCE        CREATE SEQUENCE public."Ryhmä_ryhmä_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public."Ryhmä_ryhmä_id_seq";
       public       postgres    false    4    190            �           0    0    Ryhmä_ryhmä_id_seq    SEQUENCE OWNED BY     P   ALTER SEQUENCE public."Ryhmä_ryhmä_id_seq" OWNED BY public."Ryhmä".ryhma_id;
            public       postgres    false    189            �            1259    16397    Tentti    TABLE     }   CREATE TABLE public."Tentti" (
    tentti_id bigint NOT NULL,
    ryhma_id bigint,
    tentti_nimi character varying(128)
);
    DROP TABLE public."Tentti";
       public         postgres    false    4            �            1259    16395    Tentti_ryhmä_id_seq    SEQUENCE        CREATE SEQUENCE public."Tentti_ryhmä_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public."Tentti_ryhmä_id_seq";
       public       postgres    false    4    188            �           0    0    Tentti_ryhmä_id_seq    SEQUENCE OWNED BY     P   ALTER SEQUENCE public."Tentti_ryhmä_id_seq" OWNED BY public."Tentti".ryhma_id;
            public       postgres    false    187            �            1259    16393    Tentti_tentti_id_seq    SEQUENCE        CREATE SEQUENCE public."Tentti_tentti_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public."Tentti_tentti_id_seq";
       public       postgres    false    4    188            �           0    0    Tentti_tentti_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public."Tentti_tentti_id_seq" OWNED BY public."Tentti".tentti_id;
            public       postgres    false    186            �            1259    16504    Vastaus    TABLE     �   CREATE TABLE public."Vastaus" (
    vastaus_id bigint NOT NULL,
    kysymys_id bigint NOT NULL,
    vastaus_teksti character varying(250),
    vastaus_oikein boolean DEFAULT false
);
    DROP TABLE public."Vastaus";
       public         postgres    false    4            �            1259    16502    Vastaus_kysymys_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Vastaus_kysymys_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public."Vastaus_kysymys_id_seq";
       public       postgres    false    205    4            �           0    0    Vastaus_kysymys_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public."Vastaus_kysymys_id_seq" OWNED BY public."Vastaus".kysymys_id;
            public       postgres    false    204            �            1259    16500    Vastaus_vastaus_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Vastaus_vastaus_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public."Vastaus_vastaus_id_seq";
       public       postgres    false    205    4            �           0    0    Vastaus_vastaus_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public."Vastaus_vastaus_id_seq" OWNED BY public."Vastaus".vastaus_id;
            public       postgres    false    203                       2604    16522    Kategoria kategoria_id    DEFAULT     �   ALTER TABLE ONLY public."Kategoria" ALTER COLUMN kategoria_id SET DEFAULT nextval('public."Kategoria_kategoria_id_seq"'::regclass);
 G   ALTER TABLE public."Kategoria" ALTER COLUMN kategoria_id DROP DEFAULT;
       public       postgres    false    206    207    207                       2604    16487    Kysymys kysymys_id    DEFAULT     |   ALTER TABLE ONLY public."Kysymys" ALTER COLUMN kysymys_id SET DEFAULT nextval('public."Kysymys_kysymys_id_seq"'::regclass);
 C   ALTER TABLE public."Kysymys" ALTER COLUMN kysymys_id DROP DEFAULT;
       public       postgres    false    199    202    202                       2604    16488    Kysymys tentti_id    DEFAULT     z   ALTER TABLE ONLY public."Kysymys" ALTER COLUMN tentti_id SET DEFAULT nextval('public."Kysymys_tentti_id_seq"'::regclass);
 B   ALTER TABLE public."Kysymys" ALTER COLUMN tentti_id DROP DEFAULT;
       public       postgres    false    200    202    202                       2604    16489    Kysymys kategoria_id    DEFAULT     �   ALTER TABLE ONLY public."Kysymys" ALTER COLUMN kategoria_id SET DEFAULT nextval('public."Kysymys_kategoria_id_seq"'::regclass);
 E   ALTER TABLE public."Kysymys" ALTER COLUMN kategoria_id DROP DEFAULT;
       public       postgres    false    201    202    202                       2604    16424    Käyttäjä kayttaja_id    DEFAULT     �   ALTER TABLE ONLY public."Käyttäjä" ALTER COLUMN kayttaja_id SET DEFAULT nextval('public."Käyttäjä_käyttäjä_id_seq"'::regclass);
 H   ALTER TABLE public."Käyttäjä" ALTER COLUMN kayttaja_id DROP DEFAULT;
       public       postgres    false    191    193    193                       2604    16425    Käyttäjä kayttaja_tyyppi    DEFAULT     �   ALTER TABLE ONLY public."Käyttäjä" ALTER COLUMN kayttaja_tyyppi SET DEFAULT nextval('public."Käyttäjä_käyttäjä_tyyppi_seq"'::regclass);
 L   ALTER TABLE public."Käyttäjä" ALTER COLUMN kayttaja_tyyppi DROP DEFAULT;
       public       postgres    false    193    192    193                       2604    16453    Käyttäjä-Ryhmä kayttaja_id    DEFAULT     �   ALTER TABLE ONLY public."Käyttäjä-Ryhmä" ALTER COLUMN kayttaja_id SET DEFAULT nextval('public."Käyttäjä-Ryhmä_käyttäjä_id_seq"'::regclass);
 O   ALTER TABLE public."Käyttäjä-Ryhmä" ALTER COLUMN kayttaja_id DROP DEFAULT;
       public       postgres    false    194    196    196                       2604    16454    Käyttäjä-Ryhmä ryhma_id    DEFAULT     �   ALTER TABLE ONLY public."Käyttäjä-Ryhmä" ALTER COLUMN ryhma_id SET DEFAULT nextval('public."Käyttäjä-Ryhmä_ryhmä_id_seq"'::regclass);
 L   ALTER TABLE public."Käyttäjä-Ryhmä" ALTER COLUMN ryhma_id DROP DEFAULT;
       public       postgres    false    195    196    196                       2604    16470 #   Käyttäjätyyppi kayttajatyyppi_id    DEFAULT     �   ALTER TABLE ONLY public."Käyttäjätyyppi" ALTER COLUMN kayttajatyyppi_id SET DEFAULT nextval('public."Käyttäjätyyppi_kayttajatyyppi_id_seq"'::regclass);
 T   ALTER TABLE public."Käyttäjätyyppi" ALTER COLUMN kayttajatyyppi_id DROP DEFAULT;
       public       postgres    false    198    197    198                       2604    16409    Ryhmä ryhma_id    DEFAULT     w   ALTER TABLE ONLY public."Ryhmä" ALTER COLUMN ryhma_id SET DEFAULT nextval('public."Ryhmä_ryhmä_id_seq"'::regclass);
 @   ALTER TABLE public."Ryhmä" ALTER COLUMN ryhma_id DROP DEFAULT;
       public       postgres    false    190    189    190            
           2604    16400    Tentti tentti_id    DEFAULT     x   ALTER TABLE ONLY public."Tentti" ALTER COLUMN tentti_id SET DEFAULT nextval('public."Tentti_tentti_id_seq"'::regclass);
 A   ALTER TABLE public."Tentti" ALTER COLUMN tentti_id DROP DEFAULT;
       public       postgres    false    186    188    188                       2604    16401    Tentti ryhma_id    DEFAULT     w   ALTER TABLE ONLY public."Tentti" ALTER COLUMN ryhma_id SET DEFAULT nextval('public."Tentti_ryhmä_id_seq"'::regclass);
 @   ALTER TABLE public."Tentti" ALTER COLUMN ryhma_id DROP DEFAULT;
       public       postgres    false    187    188    188                       2604    16507    Vastaus vastaus_id    DEFAULT     |   ALTER TABLE ONLY public."Vastaus" ALTER COLUMN vastaus_id SET DEFAULT nextval('public."Vastaus_vastaus_id_seq"'::regclass);
 C   ALTER TABLE public."Vastaus" ALTER COLUMN vastaus_id DROP DEFAULT;
       public       postgres    false    205    203    205                       2604    16508    Vastaus kysymys_id    DEFAULT     |   ALTER TABLE ONLY public."Vastaus" ALTER COLUMN kysymys_id SET DEFAULT nextval('public."Vastaus_kysymys_id_seq"'::regclass);
 C   ALTER TABLE public."Vastaus" ALTER COLUMN kysymys_id DROP DEFAULT;
       public       postgres    false    205    204    205            �          0    16519 	   Kategoria 
   TABLE DATA               C   COPY public."Kategoria" (kategoria_id, kategoria_nimi) FROM stdin;
    public       postgres    false    207   �m       �           0    0    Kategoria_kategoria_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public."Kategoria_kategoria_id_seq"', 4, true);
            public       postgres    false    206            �          0    16484    Kysymys 
   TABLE DATA               X   COPY public."Kysymys" (kysymys_id, tentti_id, kategoria_id, kysymys_teksti) FROM stdin;
    public       postgres    false    202   n       �           0    0    Kysymys_kategoria_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public."Kysymys_kategoria_id_seq"', 4, true);
            public       postgres    false    201            �           0    0    Kysymys_kysymys_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public."Kysymys_kysymys_id_seq"', 10, true);
            public       postgres    false    199            �           0    0    Kysymys_tentti_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public."Kysymys_tentti_id_seq"', 2, true);
            public       postgres    false    200            �          0    16421    Käyttäjä 
   TABLE DATA               w   COPY public."Käyttäjä" (kayttaja_id, kayttaja_nimi, kayttaja_salasana, kayttaja_tyyppi, kayttaja_email) FROM stdin;
    public       postgres    false    193   }n       �          0    16450    Käyttäjä-Ryhmä 
   TABLE DATA               E   COPY public."Käyttäjä-Ryhmä" (kayttaja_id, ryhma_id) FROM stdin;
    public       postgres    false    196   >o       �           0    0 %   Käyttäjä-Ryhmä_käyttäjä_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public."Käyttäjä-Ryhmä_käyttäjä_id_seq"', 1, false);
            public       postgres    false    194            �           0    0     Käyttäjä-Ryhmä_ryhmä_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public."Käyttäjä-Ryhmä_ryhmä_id_seq"', 1, false);
            public       postgres    false    195            �           0    0    Käyttäjä_käyttäjä_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public."Käyttäjä_käyttäjä_id_seq"', 21, true);
            public       postgres    false    191            �           0    0 "   Käyttäjä_käyttäjä_tyyppi_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public."Käyttäjä_käyttäjä_tyyppi_seq"', 4, true);
            public       postgres    false    192            �          0    16467    Käyttäjätyyppi 
   TABLE DATA               U   COPY public."Käyttäjätyyppi" (kayttajatyyppi_id, kayttajatyyppi_nimi) FROM stdin;
    public       postgres    false    198   [o       �           0    0 '   Käyttäjätyyppi_kayttajatyyppi_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public."Käyttäjätyyppi_kayttajatyyppi_id_seq"', 3, true);
            public       postgres    false    197            �          0    16406    Ryhmä 
   TABLE DATA               8   COPY public."Ryhmä" (ryhma_id, ryhma_nimi) FROM stdin;
    public       postgres    false    190   �o       �           0    0    Ryhmä_ryhmä_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public."Ryhmä_ryhmä_id_seq"', 1, true);
            public       postgres    false    189            �          0    16397    Tentti 
   TABLE DATA               D   COPY public."Tentti" (tentti_id, ryhma_id, tentti_nimi) FROM stdin;
    public       postgres    false    188   �o       �           0    0    Tentti_ryhmä_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public."Tentti_ryhmä_id_seq"', 7, true);
            public       postgres    false    187            �           0    0    Tentti_tentti_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public."Tentti_tentti_id_seq"', 7, true);
            public       postgres    false    186            �          0    16504    Vastaus 
   TABLE DATA               [   COPY public."Vastaus" (vastaus_id, kysymys_id, vastaus_teksti, vastaus_oikein) FROM stdin;
    public       postgres    false    205   �o       �           0    0    Vastaus_kysymys_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public."Vastaus_kysymys_id_seq"', 4, true);
            public       postgres    false    204            �           0    0    Vastaus_vastaus_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public."Vastaus_vastaus_id_seq"', 11, true);
            public       postgres    false    203            &           2606    16524    Kategoria kategoria.id 
   CONSTRAINT     b   ALTER TABLE ONLY public."Kategoria"
    ADD CONSTRAINT "kategoria.id" PRIMARY KEY (kategoria_id);
 D   ALTER TABLE ONLY public."Kategoria" DROP CONSTRAINT "kategoria.id";
       public         postgres    false    207    207                       2606    16427    Käyttäjä kayttaja.id 
   CONSTRAINT     b   ALTER TABLE ONLY public."Käyttäjä"
    ADD CONSTRAINT "kayttaja.id" PRIMARY KEY (kayttaja_id);
 E   ALTER TABLE ONLY public."Käyttäjä" DROP CONSTRAINT "kayttaja.id";
       public         postgres    false    193    193                        2606    16472 #   Käyttäjätyyppi kayttajatyyppi.id 
   CONSTRAINT     t   ALTER TABLE ONLY public."Käyttäjätyyppi"
    ADD CONSTRAINT "kayttajatyyppi.id" PRIMARY KEY (kayttajatyyppi_id);
 Q   ALTER TABLE ONLY public."Käyttäjätyyppi" DROP CONSTRAINT "kayttajatyyppi.id";
       public         postgres    false    198    198            "           2606    16494    Kysymys kysymys.id 
   CONSTRAINT     \   ALTER TABLE ONLY public."Kysymys"
    ADD CONSTRAINT "kysymys.id" PRIMARY KEY (kysymys_id);
 @   ALTER TABLE ONLY public."Kysymys" DROP CONSTRAINT "kysymys.id";
       public         postgres    false    202    202                       2606    16411    Ryhmä ryhma.id 
   CONSTRAINT     W   ALTER TABLE ONLY public."Ryhmä"
    ADD CONSTRAINT "ryhma.id" PRIMARY KEY (ryhma_id);
 =   ALTER TABLE ONLY public."Ryhmä" DROP CONSTRAINT "ryhma.id";
       public         postgres    false    190    190                       2606    16403    Tentti tentti.id 
   CONSTRAINT     Y   ALTER TABLE ONLY public."Tentti"
    ADD CONSTRAINT "tentti.id" PRIMARY KEY (tentti_id);
 >   ALTER TABLE ONLY public."Tentti" DROP CONSTRAINT "tentti.id";
       public         postgres    false    188    188            $           2606    16510    Vastaus vastaus.id 
   CONSTRAINT     \   ALTER TABLE ONLY public."Vastaus"
    ADD CONSTRAINT "vastaus.id" PRIMARY KEY (vastaus_id);
 @   ALTER TABLE ONLY public."Vastaus" DROP CONSTRAINT "vastaus.id";
       public         postgres    false    205    205            (           2606    16530    Käyttäjä kayttaja.tyyppi    FK CONSTRAINT     �   ALTER TABLE ONLY public."Käyttäjä"
    ADD CONSTRAINT "kayttaja.tyyppi" FOREIGN KEY (kayttaja_tyyppi) REFERENCES public."Käyttäjätyyppi"(kayttajatyyppi_id) NOT VALID;
 I   ALTER TABLE ONLY public."Käyttäjä" DROP CONSTRAINT "kayttaja.tyyppi";
       public       postgres    false    193    198    2080            ,           2606    16525    Kysymys kysymys.kategoria    FK CONSTRAINT     �   ALTER TABLE ONLY public."Kysymys"
    ADD CONSTRAINT "kysymys.kategoria" FOREIGN KEY (kategoria_id) REFERENCES public."Kategoria"(kategoria_id) NOT VALID;
 G   ALTER TABLE ONLY public."Kysymys" DROP CONSTRAINT "kysymys.kategoria";
       public       postgres    false    2086    207    202            +           2606    16495    Kysymys kysymys.tentti    FK CONSTRAINT     �   ALTER TABLE ONLY public."Kysymys"
    ADD CONSTRAINT "kysymys.tentti" FOREIGN KEY (tentti_id) REFERENCES public."Tentti"(tentti_id);
 D   ALTER TABLE ONLY public."Kysymys" DROP CONSTRAINT "kysymys.tentti";
       public       postgres    false    202    2074    188            )           2606    16455 "   Käyttäjä-Ryhmä liitos.kayttaja    FK CONSTRAINT     �   ALTER TABLE ONLY public."Käyttäjä-Ryhmä"
    ADD CONSTRAINT "liitos.kayttaja" FOREIGN KEY (kayttaja_id) REFERENCES public."Käyttäjä"(kayttaja_id) NOT VALID;
 P   ALTER TABLE ONLY public."Käyttäjä-Ryhmä" DROP CONSTRAINT "liitos.kayttaja";
       public       postgres    false    2078    196    193            *           2606    16460    Käyttäjä-Ryhmä liitos.ryhma    FK CONSTRAINT     �   ALTER TABLE ONLY public."Käyttäjä-Ryhmä"
    ADD CONSTRAINT "liitos.ryhma" FOREIGN KEY (kayttaja_id) REFERENCES public."Ryhmä"(ryhma_id) NOT VALID;
 M   ALTER TABLE ONLY public."Käyttäjä-Ryhmä" DROP CONSTRAINT "liitos.ryhma";
       public       postgres    false    190    196    2076            '           2606    16412    Tentti tentti.ryhmä    FK CONSTRAINT     �   ALTER TABLE ONLY public."Tentti"
    ADD CONSTRAINT "tentti.ryhmä" FOREIGN KEY (ryhma_id) REFERENCES public."Ryhmä"(ryhma_id) NOT VALID;
 B   ALTER TABLE ONLY public."Tentti" DROP CONSTRAINT "tentti.ryhmä";
       public       postgres    false    188    2076    190            -           2606    16511    Vastaus vastaus.kysymys    FK CONSTRAINT     �   ALTER TABLE ONLY public."Vastaus"
    ADD CONSTRAINT "vastaus.kysymys" FOREIGN KEY (kysymys_id) REFERENCES public."Kysymys"(kysymys_id);
 E   ALTER TABLE ONLY public."Vastaus" DROP CONSTRAINT "vastaus.kysymys";
       public       postgres    false    202    2082    205            �   ;   x�3���-�IU�M,��2�tO��M-)��2�IdTg&s�p�%�e�Tr��qqq 
�r      �   R   x�3�4BK-K{.# È�H�Ԟ��0��.�NT�N�.�T(H���/���M̳�2ʚp ����|����b���� �*      �   �   x�5̻�0 @ѹ�
�Jyn�(j}@�؊K#��^5����A��>�h��U��]ͳ���'k���M7����		�y�v#Ѱ:�ed����;�a���X)+V�q��?n�P����螛�旅��E2���'��V�C���hgM���k�Bd��NR�#�~ 	;      �      x������ � �      �   0   x�3�tL��̋��2��>�������K��9�RKJ��b���� '^�      �      x�3�t������ r�      �   !   x�3�4��M,���N�2��*�3��=... y��      �   s   x�-�A
�0D��O�����?K.�xjI�����\͛7�P�a�D0�KO��D�0r:2'����f��5�R���@����˄��|<��ڼl�7���y^µ|�]y�E�!"i     