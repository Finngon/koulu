var express = require("express")
var cors = require("cors")
const { Pool } = require('pg')
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const passport = require('passport');

//Ei tänne normaalisti
const secret = "7392429557";

var app = express()
module.exports = app
var port = process.env.PORT || 5000

app.use(express.json())
//https://expressjs.com/en/resources/middleware/cors.html

app.use(cors())

var dbConfig = {}

if (app.get('env') === 'development') {
    dbConfig = {
        user: 'postgres',
        password: 'admin',
        database: 'postgres',
        host: 'localhost',
        port: 5432,
        max: 50,
        idleTimeoutMillis: 30000
    }
} else {
    dbConfig = {
        user: 'mirokles@gmail.com',
        password: 'xxxxxx',
        database: 'ideal',
        host: 'https://tentti.azurewebsites.net/',
        port: 5432,
        max: 50,
        idleTimeoutMillis: 30000
    }
}

const pool = new Pool(dbConfig)

  pool.on('error', (err, client) => {
    console.error('Unexpected error on idle client', err)
    process.exit(-1)
  })

//  pool
//  .query('SELECT * FROM "Käyttäjä" ORDER BY kayttaja_id')
//  .then(res => console.log(res.rows[0]))
//  .catch(e => console.error(e.stack))

app.post('/addquiz', (req, res) => {
    console.log(req.body.name)
    pool
        .query(`INSERT INTO "Tentti" (tentti_nimi, ryhma_id) VALUES ($1, $2) RETURNING tentti_id;`, [req.body.name, 1])
        .then(res2 => res.send(res2.rows))
        .catch(e => console.error(e.stack))
})

app.post('/addquestion', (req, res) => {
    console.log(req.body.tentti)
    pool
        .query('INSERT INTO "Kysymys" (tentti_id, kysymys_teksti) VALUES ($1, $2) RETURNING kysymys_id', [req.body.tentti, ""])
        .then(res2 => res.send(res2.rows))
        .catch(e => console.error(e.stack))
})

app.post('/addanswer', (req, res) => {
    console.log(req.body.tentti + ', ' + req.body.kysymys)
    pool
        .query('INSERT INTO "Vastaus" (vastaus_teksti, kysymys_id, vastaus_oikein) VALUES ($1, $2, $3) RETURNING vastaus_id;', ["", req.body.kysymys, false])
        .then(res2 => res.send(res2.rows))
        .catch(e => console.error(e.stack))
})

app.delete('/deletequiz/:id', (req,res) => {
    pool
        .query('DELETE FROM "Tentti" WHERE "Tentti".tentti_id = $1', [req.params.id])
        .then(res2 => res.send(res2.rows))
        .catch(e => console.error(e.stack))
})

app.delete('/deletequestion/:id', (req,res) => {
    pool
        .query('DELETE FROM "Kysymys" WHERE "Kysymys".kysymys_id = $1', [req.params.id])
        .then(res2 => res.send(res2.rows))
        .catch(e => console.error(e.stack))
})

app.delete('/deleteanswer/:id', (req,res) => {
    pool
        .query('DELETE FROM "Vastaus" WHERE "Vastaus".vastaus_id = $1', [req.params.id])
        .then(res2 => res.send(res2.rows))
        .catch(e => console.error(e.stack))
})

app.put('/editquestion', (req, res) => {
    pool
    .query('UPDATE "Kysymys" SET "Kysymys".kysymys_teksti = $2 WHERE "Kysymys".kysymys_id = $1;', [req.body.kysymys, req.body.teksti])
    .then(res2 => res.send(res2.rows))
    .catch(e => console.error(e.stack))
})

app.get('/categories', (req,res) => {
    pool
    .query('SELECT "Kategoria".kategoria_nimi FROM "Kategoria"')
    .then(res2 => res.json(res2.rows))
    .catch(e => console.error(e.stack))
  })

app.post('/addcategory', (req, res) => {
    pool
    .query(`INSERT INTO "Kategoria" (kategoria_nimi) VALUES ($1);
            UPDATE "Kysymys" SET "Kysymys".kategoria_id VALUES (SELECT "Kategoria".kategoria_id FROM "Kategoria" WHERE "Kategoria".kategoria_nimi = $1 LIMIT 1;) WHERE kysymys_id = $2`, [req.body.kategoria, req.body.kysymys])
    .then(res2 => res.send(res2.rows))
    .catch(e => console.error(e.stack))
})

app.put('/editcategory', (req, res) => {
    pool
    .query('UPDATE "Kategoria" SET "Kategoria".kategoria_nimi = $2 WHERE "Kategoria".kategoria_id = $1;', [req.body.catid, req.body.kategoria])
    .then(res2 => res.send(res2.rows))
    .catch(e => console.error(e.stack))
})

app.put('/editanswer', (req, res) => {
    pool
    .query('UPDATE "Vastaus" SET vastaus_teksti = $2 WHERE vastaus_id = $1;', [req.body.vastaus, req.body.teksti])
    .then(res2 => res.send(res2.rows))
    .catch(e => console.error(e.stack))
})

app.put('/editcorrect', (req, res) => {
    pool
    .query('UPDATE "Vastaus" SET vastaus_oikein = $2 WHERE "Vastaus".vastaus_id = $1;', [req.body.vastaus, req.body.oikein])
    .then(res2 => console.log(res2.rows))
    .catch(e => console.error(e.stack))
})

//TODO: JSON-objektin palautus initialState-kutsuun, jotta voidaan käyttää edit- ja delete-kutsuja
const getQuery1 = `SELECT    			  "Tentti".tentti_id AS id, "Tentti".tentti_nimi AS name
                  FROM 				      "Tentti"
                  ORDER BY			    "Tentti".tentti_id ASC;`

app.get('/tentit', (req,res) => {
  pool
  .query(getQuery1)
  .then(res2 => res.json(res2.rows))
  .catch(e => console.error(e.stack))
})

const getQuery2 = `SELECT    			"Kysymys".kysymys_id AS qid, "Kysymys".kysymys_teksti AS question,
                                        "Kategoria".kategoria_nimi AS category
                    FROM 		        "Kysymys"
                    LEFT OUTER JOIN     "Kategoria" ON "Kategoria".kategoria_id = "Kysymys".kategoria_id
                    WHERE               "Kysymys".tentti_id = $1
                    ORDER BY		    "Kysymys".kysymys_id ASC;`

app.param(['id','qid','aid'], function (req, res, next, value) {
    next()
})

app.get('/tentit/:id/kysymykset', (req,res) => {
  pool
  .query(getQuery2,[req.params.id])
  .then(res2 => res.json(res2.rows))
  .catch(e => console.error(e.stack))
})

const getQuery3 = `SELECT    			  "Vastaus".vastaus_id AS aid, "Vastaus".vastaus_teksti AS answer,
                                    "Vastaus".vastaus_oikein AS isCorrect
                   FROM 				    "Vastaus"
                   WHERE            "Vastaus".kysymys_id = $1
                   ORDER BY			    "Vastaus".vastaus_id ASC;`

app.get('/tentit/:id/kysymykset/:qid/vastaukset', (req,res) => {
  pool
  .query(getQuery3,[req.params.qid])
  .then(res2 => res.json(res2.rows))
  .catch(e => console.error(e.stack))
})

app.post('/auth/register', function(req,res){

    var hashedPassword = bcrypt.hashSync(req.body.pw, 10);

    pool
    .query(`INSERT INTO "Käyttäjä" (kayttaja_nimi, 
                                    kayttaja_email, 
                                    kayttaja_salasana,
                                    kayttaja_tyyppi) 
            VALUES ($1,$2,$3,3) RETURNING kayttaja_id`,
            [req.body.nimi, req.body.email, hashedPassword])
    .then(res2 => {
        var token = jwt.sign({ id: req.body.id }, secret, {
            expiresIn: 86400 // expires in 24 hours
        })
        res.status(200).send({ auth: true, token: token })
    })
    .catch(e => res.status(500).send("There was a problem registering the user."))
})

app.post('/auth/login', function (req,res) {
    pool.query('SELECT "Käyttäjä".kayttaja_id, "Käyttäjä".kayttaja_salasana, "Käyttäjä".kayttaja_tyyppi FROM "Käyttäjä" WHERE "Käyttäjä".kayttaja_email = $1', [req.body.email])
    .then(function(r){
        if(r.rows.length === 1){
            if(bcrypt.compareSync(req.body.password, r.rows[0].kayttaja_salasana)){
                var token = jwt.sign({ id: r.rows[0].kayttaja_id }, secret, {
                    expiresIn: 86400 // expires in 24 hours
                })
                return res.status(200).send({ auth: true, token: token, mode: r.rows[0].kayttaja_tyyppi })
            } else {
                return res.status(500).send({ auth: false, message: 'Password or username does not match! (500)' });
            }
        }
    })
    .catch(e => res.status(401).send({ auth: false, message: 'Password or username does not match! (401)' }))
})

app.listen(port, () => {
    console.log("Palvelin käynnistyi portissa: " + port)
})